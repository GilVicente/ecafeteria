package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealItem;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 1130750
 */
public class MealItemPrinter implements Visitor<MealItem>{

    @Override
    public void visit(MealItem visitee) {
        System.out.printf("Meal : %s Quantity planned :%d\n",visitee.meal().toString(),visitee.qtdPlanned());
    }

    @Override
    public void beforeVisiting(MealItem visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(MealItem visitee) {
        //nothing to do
    }
    
}
