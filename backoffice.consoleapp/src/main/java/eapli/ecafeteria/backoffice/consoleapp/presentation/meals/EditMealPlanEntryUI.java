/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.EditMealPlanEntryController;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import static eapli.util.Console.readInteger;

/**
 *
 * @author 1140921
 */
public class EditMealPlanEntryUI extends AbstractUI {

    private final EditMealPlanEntryController theController = new EditMealPlanEntryController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<PlanOfMeals> allPlans = this.theController.listPlanOfMeals();

        if (!allPlans.iterator().hasNext()) {
            System.out.println("There is no plan of meals registered.");
        } else {
            System.out.println("Select Plan of Meals:");
            final SelectWidget<PlanOfMeals> selectorPlan = new SelectWidget<>(allPlans, new PlanOfMealsPrinter());
            selectorPlan.show();
            final PlanOfMeals selectedPlan = selectorPlan.selectedElement();
            if (selectedPlan != null) {
                final Iterable<MealItem> allMealItems = this.theController.listMealsItem(selectedPlan);
                if (!allMealItems.iterator().hasNext()) {
                    System.out.println("There is no meal items registered in this plan.");
                } else {
                    System.out.println("Select Meal Item:");
                    final SelectWidget<MealItem> selectorMI = new SelectWidget<>(allMealItems, new MealItemPrinter());
                    selectorMI.show();
                    final MealItem selectedMealItem = selectorMI.selectedElement();
                    if (selectedMealItem != null) {
                        int qtd = readInteger("Enter new number of dishes");
                        try {
                            theController.changeMealItem(selectedMealItem, qtd);
                        } catch (IllegalArgumentException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Edit Meal Plan Entry";
    }
}
