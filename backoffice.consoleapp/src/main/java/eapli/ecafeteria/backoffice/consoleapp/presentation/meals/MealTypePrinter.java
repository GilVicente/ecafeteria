/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.dto.GenericDTO;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 1140921
 */
public class MealTypePrinter implements Visitor<MealType> {

    MealType mealType;

    @Override
    public void visit(MealType visitee) {
        System.out.printf("%s\n", visitee.name().toLowerCase());
    }

    @Override
    public void beforeVisiting(MealType visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(MealType visitee) {
        //nothing to do
    }

}
