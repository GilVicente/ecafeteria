
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.framework.visitor.Visitor;
import static eapli.util.DateTime.format;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;


public   class PlanOfMealsPrinter implements Visitor<PlanOfMeals> {

    @Override
    public void visit(PlanOfMeals visitee) {
        System.out.printf("Inital Date : %s Final Date :%s\n",format(visitee.initialDate(),"dd/MM/yyyy"),format(visitee.finalDate(),"dd/MM/yyyy"));
    }

    @Override
    public void beforeVisiting(PlanOfMeals visitee) {

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        //nothing to do

    }

    @Override
    public void afterVisiting(PlanOfMeals visitee) {

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        //nothing to do

    }
    
}
