/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;


import eapli.ecafeteria.application.RegisterCookedMealsController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 *
 * @author Gil
 */
public class RegisterCookedMealsUI extends AbstractUI {
    
    private final RegisterCookedMealsController theController = new RegisterCookedMealsController();
    private List<MealItem> listOfMealsByDay;
    
    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    public String headline() {
        return "Register cooked meals";
    }
    
    @Override
    public boolean doShow(){
    
        listOfMealsByDay = theController.getListOfMealsByDay();
        int selectedMeal = -1;
        while(true){
        
            if(listOfMealsByDay == null){
                System.out.println("There are no meals.");
                break;
            }
            else{
                int i;
                for(i = 0; i < listOfMealsByDay.size(); i++){
                    System.out.printf("%d. Dish: %s     MealType: %s    Quantity Planned: %d\n",i+1, listOfMealsByDay.get(i).meal().dish().Name(),
                            listOfMealsByDay.get(i).meal().mealType().toString(),listOfMealsByDay.get(i).qtdPlanned());
                }
                System.out.println("0. Exit \n");
                
                try{
                    selectedMeal = Console.readInteger("Choose meal:");
                    if(selectedMeal == 0){
                        break;
                    }
                    else if(selectedMeal > listOfMealsByDay.size() || selectedMeal < 0){
                        System.out.println("Invalid meal.");
                    }
                    else{
                        try{
                            int quantityCooked = Console.readInteger("Insert Cooked Quantity:");
                            if(quantityCooked < 0){
                                System.out.println("Error.Invalid quantity.");
                            }
                            else{
                                theController.insertCookedMeals(listOfMealsByDay.get(selectedMeal-1), quantityCooked);
                                System.out.println("\nCooked Meals Registered With Sucess.\n");
                            }
                        }
                        catch(InputMismatchException exception){
                            System.out.println("Error. Invalid input.");
                        }
                    }
                }
                catch(InputMismatchException exception){
                        System.out.println("Error. Invalid input.");
                };
            }    
        }
        
        
        return false;
    }
    
}
