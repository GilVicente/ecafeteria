/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ListDishController;
import eapli.ecafeteria.application.ListDishTypeController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Tixa
 */
public class ListDishUI extends AbstractListUI<Dish>  {
    
    
    private final ListDishController theController = new ListDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<Dish> listOfElements() {
        return theController.listDish();
    }

    @Override
    protected Visitor<Dish> elementPrinter() {
        return new DishPrinter();
    }

 
    @Override
    protected String elementName() {
        return "Dish";
    }

   

   
 
}
