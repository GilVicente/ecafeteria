/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.framework.application.Controller;
import eapli.ecafeteria.application.CardChargeController;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.visitor.Visitor;
import eapli.util.Console;

/**
 *
 * @author 1140234
 */
public class CardChargeUI extends AbstractUI {

    private final CardChargeController theController = new CardChargeController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    public boolean doShow() {
        final String mecNum = Console.readLine("Input the mecanographic number:");
        theController.cafetariaUserToCharge(mecNum);

        final float credits = Float.parseFloat(Console.readLine("Input the ammount of credits:"));
        if (credits != 0) {

            theController.registerCredits(credits);
            System.out.println("Success");
        } else {
            System.out.println("You need to input an ammount.");
        }

        return true;
    }

    

    @Override
    public String headline() {
        return "Card Charge:";
    }

}
