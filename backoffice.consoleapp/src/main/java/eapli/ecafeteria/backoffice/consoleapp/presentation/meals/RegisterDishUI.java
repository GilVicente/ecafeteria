/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;


public class RegisterDishUI extends AbstractUI {
    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    public boolean doShow(){
        
        
        final Iterable<DishType> dishTypesList = this.theController.listDishTypes();
        final SelectWidget<DishType> selector = new SelectWidget<>(dishTypesList, new DishTypePrinter());
        selector.show();
        final DishType theDishType = selector.selectedElement();
               
        if (dishTypesList != null) {
			
			
                        final String name = Console.readLine("Dish name:");
                        final double price = Double.parseDouble(Console.readLine("Dish price:"));
                        final double Kcal = Double.parseDouble(Console.readLine("Dish Kcal:"));
                        final double saltQtd = Double.parseDouble(Console.readLine("Dish salt quantity:"));
                        final double sF = Double.parseDouble(Console.readLine("Dish saturated fat:"));
                        
                        try{
                        if (theDishType!=null) {
                            this.theController.registerDish(name,price,Kcal,saltQtd,sF,theDishType);
                        }else{
                            System.out.println("\n\nPlease Select a dish type!");
                        } 
                        
                         } catch (final DataIntegrityViolationException e) {
                            System.out.println("That  is already in use.");
                        }
		} else {
			System.out.
				println("\n\nDish type list is empty!");
		}
        return false;
    }
    		
    @Override
    public String headline() {
        return "Register Dish";
    }
}