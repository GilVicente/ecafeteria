/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation;

import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListBookingsAction;
import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.ListOrganicUnitsController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AcceptRefuseSignupRequestAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AddUserUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.DeactivateUserAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.ListUsersAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.AddOrganicUnitUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.OrganicUnitPrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen.RegisterCookedMealsUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ActivateDeactivateDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ChangeDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.CreateMealPlanAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.EditMealPlanAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MenuPublishingAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.RegisterDishAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.RegisterDishTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen.RegisterMealDeliveryUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.sales.CardChargeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.sales.CloseCashierAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.sales.ListNumberOfMealsPerTypeAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.sales.OpenCashDeskAction;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.actions.ReturnAction;
import eapli.framework.actions.ShowMessageAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.ListUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int LIST_USERS_OPTION = 1;
    private static final int ADD_USER_OPTION = 2;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 3;
    private static final int DEACTIVATE_USER_OPTION = 4;

    // ORGANIC UNITS
    private static final int LIST_ORGANIC_UNIT_OPTION = 1;
    private static final int ADD_ORGANIC_UNIT_OPTION = 2;

    // SETTINGS
    private static final int SET_USER_ALERT_LIMIT_OPTION = 1;
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 2;

    // DISH TYPES
    private static final int DISH_TYPE_REGISTER_OPTION = 1;
    private static final int DISH_TYPE_LIST_OPTION = 2;
    private static final int DISH_TYPE_CHANGE_OPTION = 3;
    private static final int DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION = 4;

    //DISH
    private static final int DISH_REGISTER_OPTION = 1;
    private static final int DISH_LIST_OPTION = 2;
    private static final int DISH__CHANGE_OPTION = 3;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int ORGANIC_UNITS_OPTION = 3;
    private static final int SETTINGS_OPTION = 4;
    private static final int DISH_TYPES_OPTION = 5;

    // CHANGE
    // MANAGE KITCHEN MENU
    private static final int MEALS_OPTION = 2;
    private static final int MEALS_DELIVERY_OPTION = 4;

    // CHANGE
    // KITCHEN MANAGER
    private static final int CONSULT_RESERVATIONS = 1;
    private static final int REGISTRATION_COOKED_MEALS = 2;
    private static final int REGISTRATION_COOKED_MEALS_NOT_SOLD = 3;
    private static final int EXPORT_TO_CSV_SERVED_MEALS = 4;
    private static final int EXPORT_TO_XML_SERVED_MEALS = 5;
    private static final int PREDICTION_DIFFERENCES = 6;
    private static final int REGISTRATION_MEAL_DELIVERIES = 1;

    private static final int DISH_OPTION = 6;
    private static final int BOOKING_OPTION = 3;

    //MEALS
    private static final int LIST_ALL_BOOKINGS = 1;
    private static final int CREATE_MEAL_PLAN_OPTION = 2;
    private static final int EDIT_MEAL_PLAN_OPTION = 3;
    private static final int MENU_PUBLISHING_OPTION = 4;

    //SALES
    private static final int CARD = 2;
    private static final int CARD_CHARGE = 1;
    private static final int SALES_MEALS = 3;
    private static final int LIST_NUMBER_OF_MEALS_PER_TYPE = 5;
    private static final int OPEN_DESK=4;
    private static final int CLOSE_CASH_DESK = 6;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (AppSettings.instance().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Administer)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.add(new SubMenu(USERS_OPTION, usersMenu, new ShowVerticalSubMenuAction(usersMenu)));
            final Menu organicUnitsMenu = buildOrganicUnitsMenu();
            mainMenu.add(new SubMenu(ORGANIC_UNITS_OPTION, organicUnitsMenu, new ShowVerticalSubMenuAction(organicUnitsMenu)));
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.add(new SubMenu(SETTINGS_OPTION, settingsMenu, new ShowVerticalSubMenuAction(settingsMenu)));

        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageKitchen)) {

            // CHANGE 
            final Menu kitchenManagerMenu = buildKitchenManagerMenu();
            mainMenu.add(new SubMenu(MEALS_OPTION, kitchenManagerMenu, new ShowVerticalSubMenuAction(kitchenManagerMenu)));
            final Menu myBookingMenu = buildBookingMenu();
            mainMenu.add(new SubMenu(BOOKING_OPTION, myBookingMenu, new ShowVerticalSubMenuAction(myBookingMenu)));
            final Menu kitchenMealDeliveryManagerMenu = buildKitchenDeliveryManagerMenu();
            mainMenu.add(new SubMenu(MEALS_DELIVERY_OPTION, kitchenMealDeliveryManagerMenu, new ShowVerticalSubMenuAction(kitchenMealDeliveryManagerMenu)));
            // TODO
//            throw new UnsupportedOperationException();

        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageMenus)) {
            final Menu buildMealMenu = buildMealMenu();
            mainMenu.add(new SubMenu(CREATE_MEAL_PLAN_OPTION, buildMealMenu, new ShowVerticalSubMenuAction(buildMealMenu)));
            final Menu myDishTypeMenu = buildDishTypeMenu();
            mainMenu.add(new SubMenu(DISH_TYPES_OPTION, myDishTypeMenu, new ShowVerticalSubMenuAction(myDishTypeMenu)));
            final Menu myDishMenu = buildDishMenu();
            mainMenu.add(new SubMenu(DISH_OPTION, myDishMenu, new ShowVerticalSubMenuAction(myDishMenu)));
        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Sale)) {
          
            final Menu myCardChargeMenu = buildCardChargeMenu();
            mainMenu.add(new SubMenu(CARD, myCardChargeMenu, new ShowVerticalSubMenuAction(myCardChargeMenu)));
            final Menu salesMealMenu = buildSalesMealMenu();
            mainMenu.add(new SubMenu((SALES_MEALS), salesMealMenu, new ShowVerticalSubMenuAction(salesMealMenu)));
            final Menu myOpenCashDeskMenu = buildOpenCashDesk();
            mainMenu.add(new SubMenu(OPEN_DESK, myOpenCashDeskMenu, new ShowVerticalSubMenuAction(myOpenCashDeskMenu)));
            
        }
        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }
        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.add(new MenuItem(SET_USER_ALERT_LIMIT_OPTION, "Set users' alert limit",
                new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }
    
    private Menu buildSalesMealMenu() {
        final Menu menu = new Menu("Meals >");
        menu.add(new MenuItem(LIST_NUMBER_OF_MEALS_PER_TYPE, "List number of meals available per type", new ListNumberOfMealsPerTypeAction()));
        return menu;
    }

    private Menu buildOrganicUnitsMenu() {
        final Menu menu = new Menu("Organic units >");

        menu.add(new MenuItem(LIST_ORGANIC_UNIT_OPTION, "List Organic Unit", () -> {
            // example of using the generic list ui from the framework
            new ListUI<OrganicUnit>(new ListOrganicUnitsController().listOrganicUnits(), new OrganicUnitPrinter(),
                    "Organic Unit").show();
            return false;
        }));
        menu.add(new MenuItem(ADD_ORGANIC_UNIT_OPTION, "Add Organic Unit", () -> {
            return new AddOrganicUnitUI().show();
        }));
        // TODO add other options for Organic Unit management
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.add(new MenuItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction()));
        menu.add(new MenuItem(ADD_USER_OPTION, "Add User", () -> {
            return new AddUserUI().show();
        }));
        menu.add(new MenuItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction()));
        menu.add(new MenuItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildKitchenManagerMenu() {
        final Menu menu = new Menu(" Meals >");

        menu.add(new MenuItem(REGISTRATION_COOKED_MEALS, "Register Cooked Meals", () -> {
            return new RegisterCookedMealsUI().show();
        }));

        return menu;
    }

    private Menu buildKitchenDeliveryManagerMenu() {

        final Menu menu = new Menu("Meal Delivery >");

        menu.add(new MenuItem(REGISTRATION_MEAL_DELIVERIES, "Register Meal Delivery", () -> {
            return new RegisterMealDeliveryUI().show();
        }));

        return menu;
    }

    private Menu buildDishTypeMenu() {
        final Menu menu = new Menu("Dish Type >");

        menu.add(new MenuItem(DISH_TYPE_REGISTER_OPTION, "Register new Dish Type", new RegisterDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_LIST_OPTION, "List all Dish Type", new ListDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_CHANGE_OPTION, "Change Dish Type description", new ChangeDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish Type",
                new ActivateDeactivateDishTypeAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    //menu para os dishs
    private Menu buildDishMenu() {
        final Menu menu = new Menu("Dish >");

        menu.add(new MenuItem(DISH_REGISTER_OPTION, "Register new Dish ", new RegisterDishAction()));
        menu.add(new MenuItem(DISH_LIST_OPTION, "List all Dish ", new ListDishAction()));
        // menu.add(new MenuItem(DISH_TYPE_CHANGE_OPTION, "Change Dish Type description", new ChangeDishAction()));

        return menu;
    }

    // Bookings ? shoulnd't be meal Menu ? if so add to bellow menu...
    private Menu buildBookingMenu() {
        final Menu menu = new Menu("Bookings >");

        menu.add(new MenuItem(LIST_ALL_BOOKINGS, "List all bookings", new ListBookingsAction()));

        return menu;
    }

    private Menu buildMealMenu() {
        final Menu menu = new Menu("Meal >");

        menu.add(new MenuItem(CREATE_MEAL_PLAN_OPTION, "Create meal plan entry", new CreateMealPlanAction()));
        menu.add(new MenuItem(EDIT_MEAL_PLAN_OPTION, "Edit meal plan entry", new EditMealPlanAction()));
        menu.add(new MenuItem(MENU_PUBLISHING_OPTION, "Menu Publishing", new MenuPublishingAction()));

        return menu;
    }

    private Menu buildCardChargeMenu() {
        final Menu menu = new Menu("Card >");

        menu.add(new MenuItem(CARD_CHARGE, "Card charge", new CardChargeAction()));

        return menu;
    }

    private Menu buildOpenCashDesk() {
        final Menu menu = new Menu("CashDesk >");
        menu.add(new MenuItem(CARD_CHARGE, "Open", new OpenCashDeskAction()));
        menu.add(new MenuItem(CLOSE_CASH_DESK, "Close Cash Desk", new CloseCashierAction()));
        //adicionar aqui o close cashdesk
        return menu;
    }

}
