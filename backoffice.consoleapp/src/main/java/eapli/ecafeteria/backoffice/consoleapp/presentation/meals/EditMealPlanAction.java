
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

/**
 *
 * @author 1130750
 */
public class EditMealPlanAction implements Action{

    @Override
    public boolean execute() {
        return new EditMealPlanEntryUI().show();
    }
    
}
