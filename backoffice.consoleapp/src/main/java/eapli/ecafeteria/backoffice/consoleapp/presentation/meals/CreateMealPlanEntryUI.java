/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.CreateMealPlanEntryController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.dto.GenericDTO;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author 1140921
 */
public class CreateMealPlanEntryUI extends AbstractUI {

    private final CreateMealPlanEntryController theController = new CreateMealPlanEntryController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        MealType meal = null;
        final Iterable<MealType> mealTypeList = theController.listMealTypes();
        final Calendar offDate = Console.readCalendar("Insert Date:");

        if (mealTypeList == null || !mealTypeList.iterator().hasNext()) {
            System.out.println("There is no registered Meal Type ");
        } else {
            System.out.println("Select Type of Meal:");
            final SelectWidget<MealType> selectorMealType = new SelectWidget<>(mealTypeList, new MealTypePrinter());
            selectorMealType.show();
            meal = selectorMealType.selectedElement();

            if (meal != null) {
                final Iterable<Dish> allDish = this.theController.listDishs();
                if (!allDish.iterator().hasNext()) {
                    System.out.println("There is no registered Dish ");
                } else {
                    System.out.println("Select Dish:");
                    final SelectWidget<Dish> selector = new SelectWidget<>(allDish, new DishPrinter());
                    selector.show();
                    final Dish updtDish = selector.selectedElement();
                    if (updtDish != null) {
                        final int nbrDishes = Console.readInteger("Number of  Dishes :");

                        try {
                            this.theController.createMealItem(offDate, meal, updtDish, nbrDishes);
                        } catch (DataIntegrityViolationException ex) {
                            System.out.println("ERROR");
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Create Meal Plan Entry";
    }
}
