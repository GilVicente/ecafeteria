/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.RegisterMealDeliveryController;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;

/**
 *
 * @author Tiago Lacerda
 */
public class RegisterMealDeliveryUI extends AbstractUI {

    RegisterMealDeliveryController controller = new RegisterMealDeliveryController();
    Booking b;

    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {

        String mn;
        mn = Console.readLine("Enter your Mecanographic Number: ");
        if (validateUser(mn)) {
            headline();
            showBookedBookings();
            if(this.b == null){
                return false;
            }
            controller.createListBookingController();
            controller.registerDelivery();
            controller.updateBookingState();
        } else {
            System.out.println("Invalid Mecanographic Number!");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Registered Meals:";
    }

    public void showBookedBookings() {

        ArrayList<Booking> booking = controller.getBookedBookings();
        if (!booking.isEmpty()) {
            System.out.println("Your Bookings: \n");
            for (Booking b : booking) {
                System.out.println(b.toString() + "\n");
            }
            String choice = Console.readLine("Please choose a booking:");
            for (Booking book : booking) {
                if (choice.equalsIgnoreCase(book.getMeal().toString()) || choice.equals(book.getBookingDate().toString())) {
                    this.b = book;
                    this.controller.setBooking(b);
                    break;
                }
            }

        } else {
            System.out.println("\n You currently have no booked bookings");
        }

    }

    public boolean validateUser(String mn) {
        MecanographicNumber mecaNumber = new MecanographicNumber(mn);
        return controller.validateUserID(mecaNumber);

    }

}
