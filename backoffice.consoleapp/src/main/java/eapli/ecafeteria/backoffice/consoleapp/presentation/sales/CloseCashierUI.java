/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.ecafeteria.application.CloseCashierController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealItemPrinter;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import java.util.List;

/**
 *
 * @author Daniela Ferreira
 */
public class CloseCashierUI extends AbstractUI{

    private final CloseCashierController theController = new CloseCashierController();
    
    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        
        theController.closeCashDesk();
      /*  
        final List<MealItem> deliveredMeals = theController.DeliveredMeals();
        
        if (deliveredMeals.isEmpty()) {
            System.out.println("There is no delivered Meal ");
        } else {
            
            final ListWidget<MealItem> listDeliveredMeals = new ListWidget<>(deliveredMeals, new MealItemPrinter());
            listDeliveredMeals.show();           
        }*/
        theController.LogOut();
        return false;
    }

    @Override
    public String headline() {
        return "CloseCashier";
    }
    
}
