/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.framework.actions.Action;

/**
 *
 * @author Daniela Ferreira
 */
public class CloseCashierAction implements Action{

    @Override
    public boolean execute() {
        return new CloseCashierUI().show();
    }
    
}
