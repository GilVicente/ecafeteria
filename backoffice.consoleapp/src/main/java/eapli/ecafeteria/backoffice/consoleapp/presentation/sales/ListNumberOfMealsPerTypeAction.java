/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.framework.actions.Action;

/**
 *
 * @author i141277
 */
public class ListNumberOfMealsPerTypeAction implements Action {

    @Override
    public boolean execute() {
        return new ListNumberOfMealsPerTypeUI().show();
    }

}
