/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ExtraInfoBaseController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Guilherme
 */
public abstract class ExtraInfoBaseUI extends AbstractUI {
    
   private final ExtraInfoBaseController theController = new ExtraInfoBaseController();
    
    protected Controller controller() {
        return this.theController;
    }
    
   @Override
    protected abstract boolean doShow();

   @Override
    public String headline() {
        return "Your Account Balance: "+theController.getCurrentBalance() + "Your Next Booking: "+theController.getNextReservedMeal() ;
    }
    
   @Override
    public boolean show() {
        drawFormTitle();
        final boolean wantsToExit = doShow();
        drawFormBorder();
        // Console.waitForKey("Press any key.");

        return wantsToExit;
    }
    
    
    
}
