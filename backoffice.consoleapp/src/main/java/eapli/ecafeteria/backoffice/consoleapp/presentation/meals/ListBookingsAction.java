/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListBookingsUI;
import eapli.framework.actions.Action;

/**
 *
 * @author MiguelFreitas(114127
 */
public class ListBookingsAction implements Action {

    @Override
    public boolean execute(){
        return new ListBookingsUI().show();
    }
    
}
