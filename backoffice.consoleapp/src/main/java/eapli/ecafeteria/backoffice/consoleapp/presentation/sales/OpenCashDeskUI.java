/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.ecafeteria.application.OpenCashDeskController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.visitor.Visitor;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author 1140234
 */
public class OpenCashDeskUI extends AbstractUI {

    private final OpenCashDeskController theController = new OpenCashDeskController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    public boolean doShow() {
        final Calendar offDate = Console.readCalendar("Insert Date:");

        if (theController.verifyMeal(offDate) != true) {

            System.out.println("Meals not find for that day. Select another to open the cash desk.");
            
        } else {
            theController.openCashDesk(offDate);
            System.out.println("Sucesso!");
        }

        return true;
    }

   

    @Override
    public String headline() {
        return "CashDesk";
    }

}
