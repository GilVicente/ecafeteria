/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.sales;

import eapli.ecafeteria.application.ListNumberOfMealsPerTypeController;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author i141277
 */
class ListNumberOfMealsPerTypeUI extends AbstractListUI{
    
    private final ListNumberOfMealsPerTypeController theController = new ListNumberOfMealsPerTypeController();
    
    protected ListNumberOfMealsPerTypeController controller(){
        return this.theController;
    }
    
    public boolean doShow(){
        System.out.println(theController.getNumberOfMealsPerType());
        return true;
    }

    @Override
    protected Iterable listOfElements() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Visitor elementPrinter() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String elementName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String headline(){
        return "List number of meals per type: ";
    }
}
