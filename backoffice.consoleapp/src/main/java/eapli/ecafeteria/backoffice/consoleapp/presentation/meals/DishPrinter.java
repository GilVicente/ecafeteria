/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 1140921
 */
public class DishPrinter implements Visitor<Dish> {


    DishType dishType;

    @Override
    public void visit(Dish visitee) {
        System.out.printf("Name: %s Kcal :%s Price :%s Salt quantity : %s saturated Fat:%s \n", visitee.Name(),
                String.valueOf(visitee.Kcal()), String.valueOf(visitee.Price()), String.valueOf(visitee.SaltQtd()),
                String.valueOf(visitee.SaturatedFat()));
    }

    @Override
    public void beforeVisiting(Dish visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(Dish visitee) {
    }

}
