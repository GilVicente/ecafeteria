/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

/**
 *
 * @author 1140921
 */
public class CreateMealPlanAction implements Action {

    @Override
    public boolean execute() {
        return new CreateMealPlanEntryUI().show();
    }

}
