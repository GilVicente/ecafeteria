/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.MenuPublishingController;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.visitor.Visitor;
import eapli.util.Console;
import java.util.Calendar;
import java.util.List;
import static javafx.application.Platform.exit;

/**
 *
 * @author Daniela Ferreira
 */
public class MenuPublishingUI extends AbstractUI{
    
private final MenuPublishingController theController = new MenuPublishingController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        PlanOfMeals planOfMeals = null;
        final Iterable<PlanOfMeals> planOfMealsList = theController.listPlanOfMeals();
        
        if (!planOfMealsList.iterator().hasNext()) {
            System.out.println("There is no registered Meal Type ");
        } else {
            final SelectWidget<PlanOfMeals> selectorPlanOfMeals = new SelectWidget<>(planOfMealsList, new PlanOfMealsPrinter());
            selectorPlanOfMeals.show();
            planOfMeals = selectorPlanOfMeals.selectedElement();
            if(planOfMeals!=null){
            final List<MealItem> pOfMeals = theController.planOfMeals(planOfMeals);
            final ListWidget<MealItem> listMealItem = new ListWidget<>(pOfMeals, new MealItemPrinter());
            listMealItem.show();
            
        }    
        }
        return false;
    }

    @Override
    public String headline() {
        return "Plan Of Meals";
    }
   
      
    
}
