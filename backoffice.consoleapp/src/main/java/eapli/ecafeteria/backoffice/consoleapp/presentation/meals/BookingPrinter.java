/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author MiguelFreitas(114127
 */
class BookingPrinter implements Visitor<Booking> {

    @Override
    public void visit(Booking visitee){
        System.out.println(visitee);
    }
    
    @Override
    public void beforeVisiting(Booking visitee){
        //nothing to do
    }
    
    @Override
    public void afterVisiting(Booking visitee){
        //nothing to do
    }
}
