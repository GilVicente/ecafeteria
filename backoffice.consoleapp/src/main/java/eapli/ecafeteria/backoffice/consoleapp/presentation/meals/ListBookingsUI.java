/*
 * This class represents bookings by using the AbstractListUI from the framework
 * Created by 1141277 and 1140439
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.application.ListBookingsController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author MiguelFreitas(114127
 */
public class ListBookingsUI extends AbstractListUI<Booking>{
    private final ListBookingsController theController = new ListBookingsController();
    
    protected Controller controller(){
        return this.theController;
    }
    
    @Override
    protected Iterable<Booking> listOfElements(){
        return this.theController.listBookings();
    }
    
    @Override
    protected Visitor<Booking> elementPrinter(){
        return new BookingPrinter();
    }
    
    @Override
    protected String elementName(){
        return "Booking";
    }
}
