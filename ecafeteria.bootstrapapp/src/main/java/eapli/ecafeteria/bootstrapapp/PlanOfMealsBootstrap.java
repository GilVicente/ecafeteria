/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.CreateMealPlanEntryController;
import eapli.ecafeteria.application.ListDishService;
import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.application.RegisterDishTypeController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.actions.Action;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author 1140921
 */
public class PlanOfMealsBootstrap implements Action {

    Dish dish;

    @Override
    public boolean execute() {
        this.dish = getDish();
        register(DateTime.parseDate("20-12-2016"), MealType.DINNER, this.dish, 12);
        register(DateTime.parseDate("5-12-2016"), MealType.LUNCH, this.dish, 22);
        return false;

    }

    private void register(Calendar offDate, MealType mealType, Dish dish, int qtd) {
        final CreateMealPlanEntryController controller = new CreateMealPlanEntryController();
        try {
            controller.createMealItem(offDate, mealType, dish, qtd);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }

    private Dish getDish() {

        final Set<RoleType> roles = new HashSet<RoleType>();
        roles.add(RoleType.MenuManager);
        final ListDishService dishService = new ListDishService();

        try {
            this.dish = dishService.allDishes().iterator().next();
        } catch (final Exception e) {
        }
        return this.dish;
    }

}
