/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dmaia
 */
public class DishBootstrap implements Action{
    DishType dt = null;
    @Override
    public boolean execute() {
       this.dt = getDishType();
        try {
            register("dish test 1", 2.60, 400, 6.2, 4.3, dt);
            register("dish test 2", 2.50, 320, 4.5, 7.0, dt);
        } catch (DataIntegrityViolationException ex) {

        }
        return false;
    }

    private void register(String name, double price, double Kcal, double saltQtd, double saturatedFat, DishType dishType) throws DataIntegrityViolationException {
        final RegisterDishController controller = new RegisterDishController();
        controller.registerDish(name, price, Kcal, saltQtd, saturatedFat, dishType);

    }
    
    
    private DishType getDishType() {
       
        final Set<RoleType> roles = new HashSet<RoleType>();
        roles.add(RoleType.MenuManager);
        final RegisterDishController controller = new RegisterDishController();
        try {
            dt = controller.listDishTypes().iterator().next();
        } catch (final Exception e) {
        }
        return dt;
    }
    
}
