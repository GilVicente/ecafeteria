/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Daniela Ferreira
 */
public class MenuPublishingController implements Controller {
    
    public Iterable<PlanOfMeals> listPlanOfMeals(){
        return new MenuPublishingService().allPlanOfMeals();
    }
    
    public List<MealItem> planOfMeals(PlanOfMeals planOfMeals){
        return planOfMeals.listOfMeals();
    }     
//    public PlanOfMeals listPlanOfMealsByWeek(Calendar offDate) {
//        PlanOfMealsRepository planMealsRepo = PersistenceContext.repositories().planOfMeals();
//        Calendar vecCal[]=DateTime.getBegginingAndEndOfWeek(offDate);
//        PlanOfMeals plan=planMealsRepo.findPlanOfMealsByWeek(vecCal[0].getTime(),vecCal[1].getTime());
//        return plan;
//    }    
    
    
}
