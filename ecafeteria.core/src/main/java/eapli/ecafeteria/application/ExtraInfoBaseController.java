/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;

/**
 *
 * @author Guilherme
 */
public class ExtraInfoBaseController implements Controller{
    
    private Session session;
    
    public ExtraInfoBaseController(){;
    }
 
    public double getCurrentBalance(){
        ensurePermissionOfLoggedInUser(ActionRight.SelectMeal);
        SystemUser user=session.authenticatedUser();
        Username id=user.id();
        RepositoryFactory repository = PersistenceContext.repositories();
        CafeteriaUserRepository rep = repository.cafeteriaUsers();
        CafeteriaUser cu = rep.getCafeteriaUser(id);
        return cu.seeCredits();
    }
    
    public String getNextReservedMeal(){
        ensurePermissionOfLoggedInUser(ActionRight.SelectMeal); 
        SystemUser user=session.authenticatedUser();
        Username id=user.id();
        RepositoryFactory repository = PersistenceContext.repositories();
        BookingRepository bookingRepo= repository.bookings();
        return bookingRepo.getNextBooking(id).toString();  
    }
    
}
