/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.sales.CashDesk;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Daniela Ferreira
 */
public class CloseCashierController implements Controller {

    private CashDesk cashDesk;
    private Calendar ofDate;
    private SystemUser systemUser;
    private MealItem mealItem;
    
    
    public void closeCashDesk() {
//        cashDesk.closeSales();
        ofDate = Calendar.getInstance();
        systemUser = AppSettings.instance().session().authenticatedUser();
    }

    
    public List<MealItem> DeliveredMeals() {
        List<MealItem> deliveredMealsList = new ArrayList<>();

        final RepositoryFactory repositories = PersistenceContext.repositories();
        final MealItemRepository mealItemRepo = repositories.mealItems();
        
        List<MealItem> listOfMealsByDay = mealItemRepo.getListOfMealsByDay(ofDate);
        for (MealItem Meal : listOfMealsByDay) {
            if (Meal.meal().IsDelivered()) {
                deliveredMealsList.add(Meal);

            }

        }
        return deliveredMealsList;
    }

    public int notDeliveredBookings() {
        int notDeliveredMealsQtd = 0;
        final RepositoryFactory repositories = PersistenceContext.repositories();
        final MealItemRepository mealItemRepo = repositories.mealItems();
        
        List<MealItem> listOfMealsByDay = mealItemRepo.getListOfMealsByDay(ofDate);
        for (MealItem Meal : listOfMealsByDay) {
            if (Meal.meal().IsNotDelivered()) {
                notDeliveredMealsQtd++;
            }
        }
        mealItem.QtdNotDelivered(notDeliveredMealsQtd);
        
        return notDeliveredMealsQtd;
    }
    
    public void LogOut(){
        if(systemUser==null){
            System.out.println("Não existe nenhum utilizador para fazer o logout");
        }else{
            systemUser.deactivate(ofDate);
        }
    }
    
}
