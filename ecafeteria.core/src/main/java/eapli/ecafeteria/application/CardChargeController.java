/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.sales.Charge;
import eapli.ecafeteria.domain.sales.Movement;
import eapli.ecafeteria.domain.sales.MovementType;
import static eapli.ecafeteria.domain.sales.MovementType.Charge;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author 1140234
 */
public class CardChargeController implements Controller {

    private CafeteriaUser user;

    public CardChargeController() {

    }

    public boolean registerCredits(float credits) {
        if (this.user != null) {
            user.registerCredits(credits, this.newMovementCharge());
            Charge c = this.newCharge(credits);
            return true;
        }
        return false;
    }

    public CafeteriaUser cafetariaUserToCharge(String m) {
        MecanographicNumber mec = new MecanographicNumber(m);
        CafeteriaUser cUser = PersistenceContext.repositories().cafeteriaUsers().findByMecanographincNumber(mec);

        if (cUser != null) {
            this.user = cUser;
            System.out.println("CafeteriaUser: " + this.user.mecanographicNumber().toString() + "\n");
            return cUser;
        }
        System.out.println("Not found!");
        return null;
    }

    private Movement newMovementCharge() {
        Movement m = new Movement(MovementType.Charge, this.user);
        return m;
    }

    private Charge newCharge(float credits) {
        Charge c = new Charge(this.user, credits);
        return c;
    }
    
    public void cafeteriaUser(CafeteriaUser c){
        this.user=c;
    }

}
