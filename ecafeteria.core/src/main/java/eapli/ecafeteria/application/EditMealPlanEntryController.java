/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.framework.application.Controller;
import java.util.Calendar;

/**
 *
 * @author 1140921
 */
public class EditMealPlanEntryController implements Controller {

    public MealItem changeMealItem(MealItem thisMealItem, int thisQtdPlanned) {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        thisMealItem.changeQtdPlanned(thisQtdPlanned);
        final MealItemRepository repo = PersistenceContext.repositories().mealItems();
        return repo.save(thisMealItem);

    }

    public Iterable<PlanOfMeals> listPlanOfMeals() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final PlanOfMealsRepository planOfMealsRepository = PersistenceContext.repositories().planOfMeals();
        return planOfMealsRepository.all();

    }

    public Iterable<MealItem> listMealsItem(PlanOfMeals planOfMEals) {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        return planOfMEals.listOfMeals();

    }
}
