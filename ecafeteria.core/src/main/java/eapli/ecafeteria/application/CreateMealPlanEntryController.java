package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import static eapli.util.DateTime.getBegginingAndEndOfWeek;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author 1130750
 */
public class CreateMealPlanEntryController implements Controller {

    public void createMealItem(Calendar offDate, MealType mealType, Dish dish, int qtd) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        Meal meal= new Meal(offDate, dish, mealType);
        final RepositoryFactory repoFactory=PersistenceContext.repositories();
        final MealRepository mealRepo = repoFactory.meals();
        mealRepo.add(meal);
        MealItem mealItem = new MealItem(meal, qtd);
        final MealItemRepository mealItemRepo = PersistenceContext.repositories().mealItems();
        mealItemRepo.add(mealItem);
        PlanOfMealsRepository planMealsRepo = PersistenceContext.repositories().planOfMeals();
        Calendar vecCal[]=getBegginingAndEndOfWeek(offDate);
        PlanOfMeals plan=planMealsRepo.findPlanOfMealsByWeek(vecCal[0].getTime(),vecCal[1].getTime());
        if(plan==null){
            plan= new PlanOfMeals(vecCal[0],vecCal[1]);
        }
        plan.addMealItem(mealItem);
        planMealsRepo.save(plan);
    }

    public List<MealType> listMealTypes() {
        return Arrays.asList(MealType.values());
    }
    
    public Iterable<Dish> listDishs(){
        return new ListDishService().allDishes();
    }
}