/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.ecafeteria.domain.mealbooking.BookingStatesEnum;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.UserCard;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class MealBookingController implements Controller {
    
    /**
     * Constructor
     * @param cafeteriaUser
     */
    public MealBookingController(){
    }

    /**
     * Validates if the date is a valid date
     * @param date Date
     * @param m MealType
     * @return true if the date is valid false otherwise
     */
    public boolean isValid(Calendar date, MealType m){
        Calendar currentTime = Calendar.getInstance();
        date.setLenient(false);
        try{
            date.getTime();
        }
        catch(Exception e){
            return false;
        }
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        boolean sameDay = date.get(Calendar.DAY_OF_YEAR) == currentTime.get(Calendar.DAY_OF_YEAR) &&
                date.get(Calendar.YEAR) == currentTime.get(Calendar.YEAR);
        
        return currentTime.before(date) && (!sameDay || (hour < 10 + 6 * m.ordinal()));
    }
    
    
    /**
     * Method used to create the booking and update the balance
     * @param meal meal to be used on the booking
     * @param cafeteriaUser user that is booking the meal
     * @param date date of the meal to book
      * @return return whether or not the meal was successfully booked 
     */        
    public boolean chooseMeal(Meal meal ,CafeteriaUser cafeteriaUser, Calendar date){
        UserCard userCard = cafeteriaUser.getUserCard();
        if(!isValid(date, meal.mealType())) return false;
        if(userCard.showBalance()>=  meal.dish().Price().amount()){
            userCard.setBalance(userCard.showBalance()- (float)meal.dish().Price().amount());
            Booking booking = new Booking(date, cafeteriaUser, meal);
            
            BookingState  bookinState= new BookingState(BookingStatesEnum.Booked);
            booking.setState(bookinState);
            
            return true;
        }
        return false;
    }
    /**
     * Method to filter the list of all the meals in order to return the meals desired by the user
     * @param date date used to filter the meals
     * @param mealType meal type used to filter the meals
     * @return the filtered meal list, AKA list that suits the user needs
     */
    public ArrayList<Meal> getMenu(Calendar date, MealType mealType){
        List<Meal> allMeals = PersistenceContext.repositories().meals().findByCalendar(date, mealType);
        
        ArrayList<Meal> filteredMeals = allMeals.stream()
                        .filter((Meal m) -> m!= null && date.equals(m.mealDate()))
                        .filter((Meal m) -> m!= null && mealType.equals(m.mealType()))
                        .collect(Collectors.toCollection(ArrayList::new));
        
        return filteredMeals;
    }
}
