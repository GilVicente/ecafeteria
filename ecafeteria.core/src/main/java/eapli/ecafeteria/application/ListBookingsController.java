/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author MiguelFreitas(114127
 */
public class ListBookingsController implements Controller {

    /**
     * Returns an iterable collection of all bookings present in the system.
     * @return Iterator with all bookings
     */
    public Iterable<Booking> listBookings() {
        return new ListBookingsService().allBookings();
    }

    
    /**
     * Retorna as bookings nos proximos N dias, recebidos como parametro.
     * @param days Dias de bookings que se quer retornar.
     * @return contentor com bookings pretendidas
     */
    public ArrayList<Booking> getNDaysOfBookings(int days) {
        ArrayList<Booking> b = new ArrayList<>();
        Calendar today = Calendar.getInstance();
        //Adding days to current date
        today.add(Calendar.DAY_OF_MONTH, days);
        Iterable<Booking> iterator = new ListBookingsService().allBookings();
        for (Booking booking : iterator) {
            if (booking.getBookingDate().before(today)) {
                b.add(booking);
            }
        }
        return b;
    }

    /* Método sujeito a alterações
    Tiago Lacerda 1140300 */
    public ArrayList<Booking> getUserBookings(CafeteriaUser user) {

        ArrayList<Booking> b = (ArrayList<Booking>) listBookings();
        for (Booking booking : b) {
            if (booking.getUser().equals(user)) {
                b.add(booking);
            }
            return b;
        }
        return null;
    }

}
