
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.ecafeteria.domain.mealbooking.BookingStatesEnum;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author hugoc
 */
public class CancelBookingController {
    
    public CancelBookingController(){
    
    }
    
    
    public List<String> getBookings(SystemUser user){
        List<String> bl= new ArrayList();
        bl =PersistenceContext.repositories().bookings().getBookingByUser(user);
        
        return bl;
    }
    
    public boolean cancelBooking(Booking booking, CafeteriaUser user){
        
        booking.setState(new BookingState(BookingStatesEnum.Canceled));
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        boolean sameDay = booking.getBookingDate().get(Calendar.DAY_OF_YEAR) == c.get(Calendar.DAY_OF_YEAR)
                && booking.getBookingDate().get(Calendar.YEAR) == c.get(Calendar.YEAR);
        MealType m = booking.getMeal().mealType();
            int i = (sameDay &&(hour>=10+6*m.ordinal()))? 2:1;
            user.getUserCard()
                .setBalance(user.getUserCard().getBalance()
                            +((float)booking.getMeal().dish().Price().amount()/i));
            
        
        return true;
    }
}
