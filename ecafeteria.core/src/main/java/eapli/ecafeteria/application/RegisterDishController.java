/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;

import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;


public class RegisterDishController implements Controller {
    
    
    public Dish registerDish(String name, double price, double Kcal, double saltQtd, double  saturatedFat, DishType dishType) throws DataIntegrityViolationException{
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final Dish newDish = new Dish(name,price,Kcal, saltQtd, saturatedFat,dishType);
        final DishRepository repo = PersistenceContext.repositories().dishes();
        
        repo.add(newDish);
        return newDish;
    }
    
      public Iterable<DishType> listDishTypes() {
        return new ListDishTypeService().allDishTypes();
    }

    
    
    
}
