/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.ecafeteria.domain.mealbooking.BookingStatesEnum;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.delivery.Delivery;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.application.Controller;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.DeliveryRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiago Lacerda
 */
public class RegisterMealDeliveryController implements Controller {

    final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers();
    final DeliveryRepository deliveryRepo = PersistenceContext.repositories().deliveries();
    ListBookingsController bookingsController;
    MecanographicNumber mecaNumber;
    Booking booking;
    CafeteriaUser user;

    public boolean validateUserID(MecanographicNumber mecaNumber) {

        this.mecaNumber = mecaNumber;
        return repo.findById(mecaNumber) != null;
    }

    public ListBookingsController createListBookingController() {
        return this.bookingsController = new ListBookingsController();
    }

    public ArrayList<Booking> getBookedBookings() {

        user = (CafeteriaUser) repo.findById(mecaNumber);
        
        ArrayList<Booking> bookings = bookingsController.getUserBookings(user);
        
        ArrayList<Booking> bookedBookings = null;
        for (Booking b : bookings) {
            if (b.getState().isBooked()) {
                bookedBookings.add(b);

            }
            return bookedBookings;
        }
        return null;
    }

    public void updateBookingState() {
        booking.setState(new BookingState(BookingStatesEnum.Delivered));

    }

    public boolean registerDelivery() {

        Delivery d = new Delivery(Calendar.getInstance(), user, booking);
        try {
            deliveryRepo.add(d);
        } catch (DataIntegrityViolationException ex) {
            System.out.println("CANNOT REGISTER DELIVERY!!!");
            Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

     public void setBooking(Booking b) {
        this.booking = b;
    }
}
