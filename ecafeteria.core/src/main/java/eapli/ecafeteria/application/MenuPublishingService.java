/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;

/**
 *
 * @author Daniela Ferreira
 */
class MenuPublishingService {
    
    public Iterable<PlanOfMeals> allPlanOfMeals() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final PlanOfMealsRepository planOfMealsRepository = PersistenceContext.repositories().planOfMeals();
        return planOfMealsRepository.all();
    }
    
}
