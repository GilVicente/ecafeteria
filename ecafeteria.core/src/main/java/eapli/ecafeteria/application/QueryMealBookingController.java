/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

/**
 *
 * @author dmaia
 */
public class QueryMealBookingController implements Controller{
    
    Calendar actualDate;
    Calendar limitDate;
      /**
     * Constructor
     */
    public QueryMealBookingController(){
    }
    
    
 
      /**
     * Method to filter the list of all the meals in order to return the meals desired by the user    
     * @param days
     * @return the filtered meal list, AKA list that suits the user needs
     */
    public ArrayList<Booking> getBookingPerDays (int days){
        
        this.actualDate = Calendar.getInstance();
        this.limitDate = Calendar.getInstance();
        this.limitDate.add(Calendar.DAY_OF_MONTH, days);
        
        ArrayList<Booking> allBookings= new ArrayList();
        
        final BookingRepository mealBookingRepository = PersistenceContext.repositories().bookings();
        
        final Iterable<Booking> bookingList=mealBookingRepository.all();
        
        bookingList.forEach(allBookings::add);
        
        ArrayList<Booking> filteredBookings = allBookings.stream()
                        .filter((Booking m) -> m!= null && this.actualDate.after(m.getBookingDate())) //depois da data 
                        .filter((Booking m) -> m!= null && this.limitDate.before(m.getBookingDate())) // antes da data limite
                        .collect(Collectors.toCollection(ArrayList::new));
        
        
        return filteredBookings;
        
    
    }
    
}
