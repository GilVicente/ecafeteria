/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import static eapli.util.Console.readCalendar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import static eapli.util.Console.readCalendar;

/**
 *
 * @author Gil
 */
public class RegisterCookedMealsController implements Controller {
    
    private final Calendar ofDate;
    
    public RegisterCookedMealsController(){
        ofDate = Calendar.getInstance();
    }
    
    public List<MealItem> getListOfMealsByDay(){
        final RepositoryFactory repositories = PersistenceContext.repositories();
        final MealItemRepository mealItemRepo = repositories.mealItems();
        List<MealItem> listOfMealsByDay = mealItemRepo.getListOfMealsByDay(ofDate);
        return listOfMealsByDay; 
    }
    
    
    public void insertCookedMeals(MealItem mItem, int quantityCooked){
        final RepositoryFactory repositories = PersistenceContext.repositories();
        final MealItemRepository mealItemRepo = repositories.mealItems();
        
        mItem.changeQtdEffected(quantityCooked);
        mealItemRepo.save(mItem);                                   
    }
    
    
}
