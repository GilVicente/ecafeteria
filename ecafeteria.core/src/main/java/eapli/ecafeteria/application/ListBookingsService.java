/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author MiguelFreitas(114127
 */
public class ListBookingsService {
    public Iterable<Booking> allBookings(){
        AppSettings.ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);
        
        final BookingRepository br = PersistenceContext.repositories().bookings();
        return br.all();
    }
}
