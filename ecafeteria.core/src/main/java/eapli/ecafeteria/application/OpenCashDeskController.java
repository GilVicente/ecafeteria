/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.sales.CashDesk;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author 1140234
 */
public class OpenCashDeskController implements Controller {

    private CashDesk cashDesk;

    public OpenCashDeskController() {
        this.cashDesk = new CashDesk();
    }

    public void openCashDesk(Calendar d) {
        
        if (verifyMeal(d)) {
            cashDesk.openCashDesk(this.getMeal(d));
        } else {
            System.out.println("Select another date.");
        }
    }

    private List getMeal(Calendar d) {

        MealType m = MealType.LUNCH;
        if (d.get(Calendar.HOUR) > 13) {
            m = MealType.DINNER;
        }
        List cMeal = PersistenceContext.repositories().meals().findByCalendar(d, m);

        if (!cMeal.isEmpty()) {

            return cMeal;
        }
        return null;

    }

    public boolean verifyMeal(Calendar d) {
        if (this.getMeal(d) != null) {
            return true;
        }
        return false;
    }
    
    public CashDesk returnCashDesk(){
        return this.cashDesk;
    }

}
