/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author i141277
 */
public class ListNumberOfMealsPerTypeController implements Controller {
    
    /**
     * Vai aos repositorios buscar todas as mealItems, e tedos os dish types para formar uma string
     * que explicita o numero de meals disponíveis para venda que existem para cada dishtype.
     * @return String com lista de numero de meals disponiveis para venderpor cada dishtype
     */
    public String getNumberOfMealsPerType(){
        
        AppSettings.ensurePermissionOfLoggedInUser(ActionRight.Sale);
        
        final DishTypeRepository dishTypeRepo = PersistenceContext.repositories().dishTypes();
        final MealItemRepository mealItemRepo = PersistenceContext.repositories().mealItems();
        String numberOfMealsPerType = "";
        
        Iterable<MealItem> mealItems = mealItemRepo.all();
        Iterable<DishType> dishTypes = dishTypeRepo.all();
        
        if (!mealItems.iterator().hasNext()) {
            return "There are no meal items registered!";
        }
        
        if (!dishTypes.iterator().hasNext()) {
            return "There are no registered dish types!";
        }
        
        for (DishType dishType : dishTypes) {
            numberOfMealsPerType += dishType.acronym() + ": " + getNumberOfMealsByType(mealItems, dishType)+"\n";
        }
        return numberOfMealsPerType;
    }

    /**
     * Método que percorre MealItems e retorna quantas meals existem para venda de um certo dishtype
     * @param mealItems mealItems para percorrer
     * @param dishType dishtype para testar
     * @return retorna o numero de meals disponiveis para vender.
     */
    private int getNumberOfMealsByType(Iterable<MealItem> mealItems, DishType dishType) {
        int numberOfOccurrences = 0;
        for (MealItem mealItem : mealItems) {
            if (mealItem.meal().dish().DishType().equals(dishType)) {
                //effected - sold - reserved são as dispoíveis.
                numberOfOccurrences += mealItem.qtdEffected()-mealItem.qtdDelivered()-mealItem.qtdReserved();
            }
        }
        return numberOfOccurrences;
    }
    
}
