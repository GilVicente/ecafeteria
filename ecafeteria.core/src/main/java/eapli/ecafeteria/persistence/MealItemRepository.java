/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author 1140921
 */


public interface MealItemRepository extends Repository<MealItem, Long> {
    
    public List getMealItemByData(Calendar c, MealType m);
    public List getListOfMealsByDay(Calendar ofDate);
    public List getDeliveredMeals();
}

