/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author 1140921
 */
public class InMemoryMealItemRepository extends InMemoryRepository<MealItem, Long> implements MealItemRepository {

    long nextID = 1;

    @Override
    protected Long newPK(MealItem entity) {
        return ++nextID;
    }

    @Override
    public List getListOfMealsByDay(Calendar ofDate) {
        List<MealItem> listOfMealsByDay = new ArrayList<>();
        for (MealItem mItem : repository.values()) {
            if (mItem.meal().mealDate().get(Calendar.YEAR) == ofDate.get(Calendar.YEAR)
                    && mItem.meal().mealDate().get(Calendar.DAY_OF_YEAR) == ofDate.get(Calendar.DAY_OF_YEAR)) {
                listOfMealsByDay.add(mItem);
            }
        }
        if (listOfMealsByDay.isEmpty()) {
            return null;
        } else {
            return listOfMealsByDay;
        }
    }

    public List getDeliveredMeals() {
        List<MealItem> deliveredMealsList = new ArrayList<>();
        for (MealItem mItem : repository.values()) {
            if (mItem.meal().IsDelivered()) {
                deliveredMealsList.add(mItem);
            }
        }
        return deliveredMealsList;
    }

    @Override
    public List getMealItemByData(Calendar c, MealType m) {

        List listmpi = new ArrayList<>();
        Iterator it = all().iterator();
        while (it.hasNext()) {
            MealItem mpi = (MealItem) it.next();
            if (mpi.meal().mealDate().equals(Calendar.DAY_OF_WEEK) && mpi.meal().mealType().equals(m)) {
                listmpi.add(mpi);
            }
        }
        return listmpi;
    }
}
