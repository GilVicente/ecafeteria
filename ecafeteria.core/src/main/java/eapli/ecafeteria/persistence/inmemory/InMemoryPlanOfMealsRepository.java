/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

/**
 *
 * @author Daniela Ferreira
 */
public class InMemoryPlanOfMealsRepository extends InMemoryRepository<PlanOfMeals, Long> implements PlanOfMealsRepository {

    long nextID = 1;

    @Override
    protected Long newPK(PlanOfMeals entity) {
        return ++nextID;
    }

    @Override
    public PlanOfMeals findPlanOfMealsByWeek(Date weekStart, Date weekEnd) {
        for (PlanOfMeals plan : repository.values()) {
            if (plan.initialDate().getTime().equals(weekStart) && plan.finalDate().getTime().equals(weekEnd)) {
                return plan;
            }
        }
        return null;
    }
}
