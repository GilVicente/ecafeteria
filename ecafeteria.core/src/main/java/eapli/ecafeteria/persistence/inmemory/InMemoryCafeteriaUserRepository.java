package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.EmailAddress;
import eapli.ecafeteria.domain.authz.Name;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.Role;
import eapli.ecafeteria.domain.authz.RoleSet;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.mealbooking.UserCard;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 * 02/04/2016
 */
public class InMemoryCafeteriaUserRepository extends InMemoryRepository<CafeteriaUser, MecanographicNumber> implements CafeteriaUserRepository {

    @Override
    protected MecanographicNumber newPK(CafeteriaUser u) {
        return u.id();
    }

 

    

    
    //not yet implemented
    @Override
    public CafeteriaUser getCafeteriaUser(Username id) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    @Override
    public CafeteriaUser findBySystemUser(SystemUser user) {
        for (CafeteriaUser plan : repository.values()) {
            if (plan.systemUser().equals(user)) {
                return plan;
            }
        }
        return null;
    }

    @Override
    public CafeteriaUser findByMecanographincNumber(MecanographicNumber m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
