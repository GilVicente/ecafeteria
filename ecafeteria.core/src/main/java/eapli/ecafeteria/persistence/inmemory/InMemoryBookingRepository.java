/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MiguelFreitas(114127
 */
public class InMemoryBookingRepository extends InMemoryRepository<Booking, Long> implements BookingRepository{

    long nextID = 1;
    
    @Override
    protected Long newPK(Booking entity) {
        return ++nextID;
    }
    
    //not implemented yet
    @Override
    public Booking getNextBooking(Username id) {
        throw new UnsupportedOperationException("Not implemented yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<String> getBookingByUser(SystemUser user) {
        List <String> bl = new ArrayList();
         for (Booking booking : repository.values()) {
            if (booking.getUser().systemUser().equals(user)) {
                bl.add(booking.getBookingDate().toString()+"   ");
            }
        }
         return bl;
    }
}
