package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.DeliveryRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealItemRepository;

import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static PlanOfMealsRepository planOfMealRepository = null;
    private static UserRepository userRepository = null;
    private static DishTypeRepository dishTypeRepository = null;
    private static DishRepository dishRepository = null;
    private static OrganicUnitRepository organicUnitRepository = null;
    private static CafeteriaUserRepository cafeteriaUserRepository = null;
    private static SignupRequestRepository signupRequestRepository = null;
    private static BookingRepository bookingRepository = null;
    private static MealRepository mealRepository = null;
    private static MealItemRepository mealItemRepository = null;
    private static DeliveryRepository deliveryRepository = null;

    @Override
    public UserRepository users() {
        if (userRepository == null) {
            userRepository = new InMemoryUserRepository();
        }
        return userRepository;
    }

    @Override
    public DishTypeRepository dishTypes() {
        if (dishTypeRepository == null) {
            dishTypeRepository = new InMemoryDishTypeRepository();
        }
        return dishTypeRepository;
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        if (organicUnitRepository == null) {
            organicUnitRepository = new InMemoryOrganicUnitRepository();
        }
        return organicUnitRepository;
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers() {

        if (cafeteriaUserRepository == null) {
            cafeteriaUserRepository = new InMemoryCafeteriaUserRepository();
        }
        return cafeteriaUserRepository;
    }

    @Override
    public SignupRequestRepository signupRequests() {

        if (signupRequestRepository == null) {
            signupRequestRepository = new InMemorySignupRequestRepository();
        }
        return signupRequestRepository;
    }

    @Override
    public DishRepository dishes() {
        if (dishRepository == null) {
            dishRepository = new InMemoryDishRepository();
        }
        return dishRepository;
    }

    @Override
    public BookingRepository bookings() {
        if (bookingRepository == null) {
            bookingRepository = new InMemoryBookingRepository();
        }
        return bookingRepository;
    }

    public MealRepository meals() {
        if (mealRepository == null) {
            mealRepository = new InMemoryMealRepository();
        }
        return mealRepository;
    }

    @Override
    public MealItemRepository mealItems() {
        if (mealItemRepository == null) {
            mealItemRepository = new InMemoryMealItemRepository();
        }
        return mealItemRepository;

    }

    @Override
    public PlanOfMealsRepository planOfMeals() {

        if (planOfMealRepository == null) {
            planOfMealRepository = new InMemoryPlanOfMealsRepository();
        }
        return planOfMealRepository;
    }

    @Override
    public DeliveryRepository deliveries() {
           if (deliveryRepository == null) {
           deliveryRepository = new InMemoryDeliveryRepository();
        }
        return deliveryRepository;

    }
}



