package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author 1130750
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal, Long> implements MealRepository{
    
    long nextID = 1;
    
    @Override
    protected Long newPK(Meal entity) {
        return ++nextID;
    }
 

    @Override
    public List findByCalendar(Calendar d, MealType m) {
        List lst = new ArrayList();
        for (Meal mItem : repository.values()) {
            if (mItem.mealDate().get(Calendar.DAY_OF_YEAR) == d.get(Calendar.DAY_OF_YEAR) && mItem.mealType().equals(m)) {
                lst.add(mItem);
            }
        }
        return null;
    }
}
