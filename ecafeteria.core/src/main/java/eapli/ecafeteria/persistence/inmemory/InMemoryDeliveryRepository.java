/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.delivery.Delivery;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.DeliveryRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Tiago Lacerda
 */
public class InMemoryDeliveryRepository extends InMemoryRepository<Delivery, Long> implements DeliveryRepository {

    long nextID = 1;
    
//    @Override
//    protected Long newPK(Booking entity) {
//        return ++nextID;
//    }

    @Override
    protected Long newPK(Delivery entity) {
        return ++nextID;
    }
    
}
