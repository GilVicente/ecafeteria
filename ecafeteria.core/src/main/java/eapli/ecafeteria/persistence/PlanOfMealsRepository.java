/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;



import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Daniela Ferreira
 */
public interface PlanOfMealsRepository extends Repository<PlanOfMeals, Long>{
    
    public PlanOfMeals findPlanOfMealsByWeek(Date weekStart,Date weekEnd);
   
}
