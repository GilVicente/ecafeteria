/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.framework.persistence.repositories.Repository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MiguelFreitas(114127
 */
public interface BookingRepository extends Repository<Booking, Long>{
    
    public Booking getNextBooking(Username id);
    
    
    public List<String> getBookingByUser(SystemUser user);
}
