package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.persistence.repositories.Repository;

/**
 * Created by MCN on 29/03/2016.
 */
public interface DishRepository extends Repository<Dish, Long> {


}
