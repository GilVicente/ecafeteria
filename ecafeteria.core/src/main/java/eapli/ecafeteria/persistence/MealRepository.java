package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author 1130750
 */
public interface MealRepository extends Repository<Meal, Long> {

    public List findByCalendar(Calendar d, MealType m);
    
    
}
