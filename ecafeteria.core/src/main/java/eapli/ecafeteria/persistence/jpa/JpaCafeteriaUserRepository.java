package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt 02/04/2016
 */
class JpaCafeteriaUserRepository extends JpaRepository<CafeteriaUser, MecanographicNumber>
        implements CafeteriaUserRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    public CafeteriaUser findBySystemUser(SystemUser user) {
        Query q = entityManager().createNamedQuery("SELECT e FROM CafeteriaUser e where e.systemUser=:p")
                .setParameter("p", user);
        List<CafeteriaUser> list = q.getResultList();
        if (list.size() != 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public CafeteriaUser findByMecanographincNumber(MecanographicNumber m) {
       
        //erro
        Query q = entityManager().createNamedQuery("SELECT e FROM CafeteriaUser e WHERE e.mecanographicNumber =:p").setParameter("p", m);
        
        List<CafeteriaUser> list = q.getResultList();
        if (list.size() != 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public CafeteriaUser getCafeteriaUser(Username id) {
        Query cu = entityManager().createQuery("SELECT cu FROM CafeteriaUser cu WHERE :id = cu.systemUser.username");
        cu.setParameter("id", id);
        CafeteriaUser c;
        c = (CafeteriaUser) cu.getSingleResult();
        return c;
    }
}
