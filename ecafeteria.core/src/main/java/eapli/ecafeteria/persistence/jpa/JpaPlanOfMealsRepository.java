/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.PlanOfMeals;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Query;

/**
 *
 * @author Daniela Ferreira
 */
class JpaPlanOfMealsRepository extends JpaRepository<PlanOfMeals, Long> implements PlanOfMealsRepository{

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public PlanOfMeals findPlanOfMealsByWeek(Date weekStart, Date weekEnd) {
        String queryStr="SELECT plan FROM PlanOfMeals plan where plan.initialDate=:p1 and plan.finalDate=:p2";       		
        Query query = entityManager().createQuery(queryStr).setParameter("p1",weekStart).setParameter("p2",weekEnd);
        if(query.getResultList().isEmpty()){
            return null;
        }
        PlanOfMeals plan = (PlanOfMeals)query.getResultList().get(0);
        return  plan;
    }
    
}
