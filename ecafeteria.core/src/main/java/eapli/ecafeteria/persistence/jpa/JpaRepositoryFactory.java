package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.DeliveryRepository;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PlanOfMealsRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users() {
        return new JpaUserRepository();
    }

    @Override
    public JpaDishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public JpaOrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers() {
        return new JpaCafeteriaUserRepository();
    }
    
        @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository();
    }

   @Override
    public JpaDishRepository dishes() {
        return new JpaDishRepository();
    }

    @Override
    public BookingRepository bookings() {
        return new JpaBookingRepository();
    }

    @Override

    public MealRepository meals() {
        return new JpaMealRepository();
    }

    @Override
    public MealItemRepository mealItems() {
        return new JpaMealItemRepository();
        
    }
    public PlanOfMealsRepository planOfMeals() {
        return new JpaPlanOfMealsRepository();
    }

    @Override
    public DeliveryRepository deliveries() {
        return new JpaDeliveryRepository();
    }
}
