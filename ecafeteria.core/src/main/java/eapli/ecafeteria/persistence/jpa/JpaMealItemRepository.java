/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealItemRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Filipe
 */
public class JpaMealItemRepository extends JpaRepository<MealItem, Long> implements MealItemRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List<MealItem> getListOfMealsByDay(Calendar ofDate) {
        List<MealItem> mealItems = all();
        if (mealItems.isEmpty()) {
            return null;
        }
        List<MealItem> listOfMealsByDay = new ArrayList<>();
        for (MealItem mItem : mealItems) {
            if (mItem.meal().mealDate().get(Calendar.YEAR) == ofDate.get(Calendar.YEAR)
                    && mItem.meal().mealDate().get(Calendar.DAY_OF_YEAR) == ofDate.get(Calendar.DAY_OF_YEAR)) {
                listOfMealsByDay.add(mItem);
            }
        }
        if (listOfMealsByDay.isEmpty()) {
            return null;
        } else {
            return listOfMealsByDay;
        }
    }

    @Override
    public List getDeliveredMeals() {
        List<MealItem> mealItem = all();
        if (mealItem.isEmpty()) {
            return null;
        }
        List<MealItem> deliveredMealsList = new ArrayList<>();
        for (MealItem mItem : mealItem) {
            if (mItem.meal().IsDelivered()) {
                deliveredMealsList.add(mItem);
            }
        }
        return deliveredMealsList;
    }

    @Override
    public List getMealItemByData(Calendar c, MealType m) {
        System.out.println("------------------------>entrou1");
        EntityManager em = entityManager();
        System.out.println("------------------------>entrou2");
        Query q = em.createQuery(
                "SELECT mi FROM MealItem mi WHERE mi.meal.mealDate =:date AND mi.meal.mealType =:type").setParameter("date", c).setParameter("type", m);
        List meals = q.getResultList();
        em.close();
        return meals;

    }

}
