/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author MiguelFreitas(114127
 */
public class JpaBookingRepository extends JpaRepository<Booking, Long> implements BookingRepository{
    @Override
    public String persistenceUnitName(){
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Booking getNextBooking(Username id) {
        List<Booking> bookings = new ArrayList<>();
        EntityManager em = entityManagerFactory().createEntityManager();
        Query query = entityManager().createQuery("Select id from Booking where Booking.state like 'Booked' and Booking.user in (Select user from CafeteriaUser WHERE"+id.toString()+"like CafeteriaUser.SYSYEMUSER_USERNAME");
        query.setParameter("id", id);
        if(bookings.isEmpty()){
            return null;
        }
        bookings = query.getResultList();
        return bookings.iterator().next();
    }

    @Override
    public List<String> getBookingByUser(SystemUser user) {
        List<String> bookings = new ArrayList<>();
        List<String> date = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        Booking b = new Booking();
        EntityManager em = entityManagerFactory().createEntityManager();
        Query query = entityManager().createQuery("Select DATE from Booking where Booking.state like 'Booked' and Booking.user in (Select user from CafeteriaUser WHERE user.SYSTEMUSER_USERNAME=:p)");
        query.setParameter("p", user.username().toString());
        Query query2 = entityManager().createQuery("Select id from Booking where Booking.state like 'Booked' and Booking.user in (Select user from CafeteriaUser WHERE user.SYSTEMUSER_USERNAME=:p)");
        query.setParameter("p", user.username().toString());
        
        date=  query.getResultList();
        ids = query2.getResultList();
        
        for (int i = 0; i < date.size(); i++) {
           bookings.add(date.get(i)+"  "+ids.get(i));
        }
        if(bookings.isEmpty()){
            return null;
        }
        bookings = query.getResultList();
        return bookings;
    }
}
