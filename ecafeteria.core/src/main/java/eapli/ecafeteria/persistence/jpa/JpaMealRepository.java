package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author 1130750
 */
public class JpaMealRepository extends JpaRepository<Meal, Long> implements MealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List findByCalendar(Calendar c, MealType m) {
       
        EntityManager em = entityManager();
        
        
        //erro caso o calendar guarde horas e minutos, daí não encontrar as meals
        Query q = em.createQuery(
                "SELECT m FROM Meal m WHERE m.ofDate =:date AND m.mealType =:type").setParameter("date", c).setParameter("type", m);
        List meals = q.getResultList();
        em.close();
        return meals;
    }

}
