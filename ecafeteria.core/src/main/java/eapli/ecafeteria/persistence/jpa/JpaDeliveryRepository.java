/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.delivery.Delivery;
import eapli.ecafeteria.persistence.DeliveryRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author Tiago Lacerda
 */
public class JpaDeliveryRepository extends JpaRepository<Delivery, Long> implements DeliveryRepository{

    @Override
    protected String persistenceUnitName() { 
        
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    
    
}
