/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.delivery.Delivery;
import eapli.framework.persistence.repositories.Repository;



/**
 *
 * @author Tiago Lacerda
 */
public interface DeliveryRepository extends Repository<Delivery, Long>{

   
    
}
