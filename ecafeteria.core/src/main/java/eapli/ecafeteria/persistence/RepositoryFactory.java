/**
 *
 */
package eapli.ecafeteria.persistence;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {
        PlanOfMealsRepository planOfMeals();
	UserRepository users();
	DishTypeRepository dishTypes();
        DishRepository dishes();
        OrganicUnitRepository organicUnits();
        CafeteriaUserRepository cafeteriaUsers();
        SignupRequestRepository signupRequests();
        BookingRepository bookings();
        MealRepository meals();
        MealItemRepository mealItems();
        DeliveryRepository deliveries();
}
