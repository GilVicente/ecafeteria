/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.sales;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author 1140234
 */
@Entity
public class CashDesk {
    
    @Id
    @GeneratedValue
    long id;
    
    @OneToMany
    private List mealToWork;
    
    private boolean isOpen;
   


    public CashDesk(){
        
    }
    
    public void closeSales(){
        setCashDeskClose();
        
        
    }
    
    public void openCashDesk(List m){
        
        setMealToWork(m);
        setCashDeskOpen();
        
    }
    
    
    
    private void setMealToWork(List m){
        this.mealToWork=m;
    }
    
    private void setCashDeskOpen(){
        this.isOpen=true;
    }
    
    private void setCashDeskClose(){
        this.isOpen=false;
    }
    
    public boolean cashIsOpenOrNot(){
        return this.isOpen;
    }
}
