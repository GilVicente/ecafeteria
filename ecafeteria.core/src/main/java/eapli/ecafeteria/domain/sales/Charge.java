/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.sales;

import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;




@Entity
public class Charge {
    @Id
    private CafeteriaUser userCharge;
    
    @OneToOne(cascade = CascadeType.MERGE)
    private float chargedAmount;
    
    
    public Charge( CafeteriaUser id, float cA) {
        this.userCharge= id;
        this.chargedAmount=cA;
    }
    
    public String toString(){
        return "ID: "+this.userCharge.mecanographicNumber();
    }
}
