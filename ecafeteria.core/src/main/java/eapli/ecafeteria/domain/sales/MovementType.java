/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.sales;

import javax.persistence.Entity;

/**
 *This enum represent the types of movements
 * @author 1140234
 */


public enum MovementType {
    Repayment,
    Buy,
    Charge;
}
