/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.sales;

import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.UserCard;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *Movement class its were can be created new movements
 * @author 1140234
 */

@Entity
public class Movement implements Serializable {
    
    @Id
    private CafeteriaUser userMovement;
    
    @Enumerated(EnumType.STRING)
    private MovementType mov;
    
    public Movement(MovementType type, CafeteriaUser id) {
        this.mov=type;
        this.userMovement= id;
    }
    
    public String toString(){
        return "ID: "+this.userMovement.mecanographicNumber()+"\nMovimento: " + mov.toString();
    }
    
}
