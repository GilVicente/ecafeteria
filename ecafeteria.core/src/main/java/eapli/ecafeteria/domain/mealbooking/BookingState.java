package eapli.ecafeteria.domain.mealbooking;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BookingState implements AggregateRoot<Long>, Serializable {

    private BookingStatesEnum state;

    @Id
    @GeneratedValue
    private long id;

    public BookingState() {

    }

    public BookingState(BookingStatesEnum state) {
        this.state = state;
    }

    public BookingStatesEnum getState() {
        return this.state;
    }

    public boolean isBooked() {

        return this.state == BookingStatesEnum.Booked;
    }

    public boolean isDelivered() {
        return this.state == BookingStatesEnum.Delivered;
    }

    public boolean isNotDelivered() {
        return this.state == BookingStatesEnum.Not_Delivered;
    }

    @Override
    public boolean sameAs(Object other) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean is(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long id() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
