/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.sales.Movement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Embeddable;

/**
 *
 * @author Tiago
 */
@Embeddable
public class UserCard implements Serializable {
     
    private float balance;
    private List<Movement> lstMov;
   
   
    public UserCard() {
        this.balance = 0;
        this.lstMov = new ArrayList<Movement>();
        
    }

    private void addMovementCharge(Movement m) {
        this.lstMov.add(m);
    }

    private void addCredits(float credits) {
        setBalance(getBalance() + credits);
    }

    public void cardMovements() {
        for (Movement m : this.lstMov) {
            System.out.println("Movimento: " + m.toString());
        }
    }

    /**
     * Public method that adds credits to a card and registers the movement
     *
     * @param credits
     */
    public void registerCredits(float credits, Movement m) {
        this.addCredits(credits);
        this.addMovementCharge(m);
    }

    /**
     * @param balance the balance to set
     */
    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getBalance() {
        return balance;
    }
    
    public float showBalance(){
        return this.getBalance();
    }

    @Override
    public String toString() {
        return "Saldo: " + this.getBalance();
    }
}
