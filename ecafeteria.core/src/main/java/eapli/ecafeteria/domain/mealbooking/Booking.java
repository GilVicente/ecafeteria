package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Booking {

    @Id
    @GeneratedValue
    private long id;

    @Temporal(TemporalType.DATE)
    private Calendar BookingDate;
    @OneToOne
    private Meal meal;
    @ManyToOne
    private CafeteriaUser user;
    @OneToOne(cascade = CascadeType.ALL)
    private BookingState state;

    public Booking() {
        //for JPA
    }

    /**
     * Creates a Booking.
     * @param BookingDate Date of Booking.
     * @param meal Meal of Booking.
     * @param user User that books the Booking.
     * @param state Initial Booking State of the Booking.
     */
    public Booking(Calendar BookingDate, CafeteriaUser user, Meal meal) {
        this.BookingDate = BookingDate;
        this.meal = meal;
        this.user = user;
        this.state = new BookingState(BookingStatesEnum.Booked);
        
        this.updateBalance();
    }
    
    public Calendar getBookingDate() {
        return BookingDate;
    }

    public CafeteriaUser getUser() {
        return user;
    }

    public BookingState getState() {
        return state;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setBookingDate(Calendar BookingDate) {
        this.BookingDate = BookingDate;
    }

    public void setUser(CafeteriaUser user) {
        this.user = user;
    }

    public void setState(BookingState state) {
        this.state = state;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }
    
    
    public void updateBalance(){
        //TODO updates the user's balance
    }
    
    
    @Override
    public String toString(){
        String toString;
        toString = "Booking date: "+BookingDate
                    +"\nUser: "+user
                    +"\nMeal: "+meal
                    +"\nState: "+state;
        return toString;
    }
}