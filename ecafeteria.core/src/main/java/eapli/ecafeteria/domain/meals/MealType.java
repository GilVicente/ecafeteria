
package eapli.ecafeteria.domain.meals;

/**
 *
 * @author 1130750
 */
public enum  MealType {
    LUNCH,DINNER
}