/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Money;
import eapli.util.Strings;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Dish implements AggregateRoot<Long>, Serializable{
   
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    
    private String name;
    private Money price;
    private double Kcal;
    private double saltQtd;
    private double saturatedFat;
    private DishType dishType;

    protected Dish() {
        // for ORM
    }

    public Dish(String name, double price, double Kcal, double saltQtd, double  saturatedFat, DishType dishtype) {
        if (Strings.isNullOrEmpty(name)) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.Kcal= Kcal;     
        this.saltQtd=saltQtd;
        this.saturatedFat=saturatedFat;
        this.price= Money.euros(price);
        this.dishType=dishtype;
        
    }

    public String Name() {
        return this.name;
    }

    public Money Price() {
        return this.price;
    }

    public double Kcal() {
        return this.Kcal;
    }

    public double SaltQtd() {
        return this.saltQtd;
    }

    public double SaturatedFat() {
        return this.saturatedFat;
    }

    public DishType DishType(){
        return this.dishType;
    }
    // FIXME implement equals() and hashCode()

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(Long id) {
         return id.equals(this.id);
    }

    @Override
    public Long id() {
         return this.id;
    }
}
