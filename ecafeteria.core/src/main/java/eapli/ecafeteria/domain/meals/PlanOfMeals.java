/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import org.eclipse.persistence.jpa.config.Cascade;

/**
 *
 * @author Daniela Ferreira
 */
@Entity
public class PlanOfMeals implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar initialDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar finalDate;
    @OneToMany(cascade=CascadeType.MERGE)
    private List<MealItem> listOfMeals;

    public Long getId() {
        return id;
    }

    public PlanOfMeals() {
    }

    public PlanOfMeals(Calendar initialDate, Calendar finalDate) {
        this.listOfMeals = new ArrayList<>();
        this.initialDate = initialDate;
        this.finalDate = finalDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar initialDate() {
        return this.initialDate;
    }

    public Calendar finalDate() {
        return this.finalDate;
    }

    public void addMealItem(MealItem mealItem) {
        this.listOfMeals.add(mealItem);
    }

    public void changeDate(Calendar newInitialDate, Calendar newFinalDate) {
        if (newInitialDate.after(newFinalDate) || newFinalDate.before(newInitialDate)) {
            throw new IllegalArgumentException();
        }
        this.initialDate = newInitialDate;
        this.finalDate = newFinalDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanOfMeals)) {
            return false;
        }
        PlanOfMeals other = (PlanOfMeals) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eapli.ecafeteria.domain.meals.PlanOfMeals[ id=" + id + "  ]";
    }

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(Long id) {
        return id.equals(this.id);
    }

    @Override
    public Long id() {
        return this.id;
    }
    public List<MealItem> listOfMeals(){
        return this.listOfMeals;
    }

}
