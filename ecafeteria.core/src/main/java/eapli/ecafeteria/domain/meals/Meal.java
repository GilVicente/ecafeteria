package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.framework.domain.AggregateRoot;
import static eapli.util.DateTime.format;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1140921
 */
@Entity

public class Meal implements AggregateRoot<Long>, Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar ofDate;
    @OneToOne(cascade = CascadeType.MERGE)
    private Dish dish;
    private MealType mealType;
    private BookingState delivered;
    private BookingState notDelivered;

    public Meal() {
    }

    public Meal(Calendar ofDate, Dish dish, MealType mealType) {
        this.ofDate = ofDate;
        this.dish = dish;
        this.mealType = mealType;
    }

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(Long id) {
        return id.equals(this.id);
    }

    @Override
    public Long id() {
        return this.id;
    }

    public Calendar mealDate() {
        return this.ofDate;
    }

    public Dish dish() {
        return this.dish;
    }

    public boolean IsDelivered() {
        return this.delivered.isDelivered();
    }

    public boolean IsNotDelivered() {
        return this.notDelivered.isNotDelivered();
    }

    public MealType mealType() {
        return this.mealType;
    }

    public void changeMeal(Calendar newOfDate, Dish newDish, MealType newMealType) {
        if (dish == null || mealType == null) {
            throw new IllegalStateException();

        }
        this.ofDate = newOfDate;
        this.dish = newDish;
        this.mealType = newMealType;

    }

    @Override
    public String toString() {
        return "Date: " + format(ofDate, "dd/MM/yyyy") + " Dish: " + dish().Name() + " Meal Type: " + mealType.toString().toLowerCase();
    }
}
