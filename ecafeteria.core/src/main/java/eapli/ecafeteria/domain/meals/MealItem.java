package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author 1130750
 */
@Entity
public class MealItem implements AggregateRoot<Long>, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @OneToOne(cascade=CascadeType.MERGE)
    private Meal meal;
    private int qtdPlanned;
    private int qtdEffected;
    private int qtdReserved;
    private int qtdDelivered;
    private int qtdNotDelivered;
    private final static int QTD_PLANNED_BY_OMISSION = -1;
    private final static int QTD_EFFECTED_BY_OMISSION = -1;
    private final static int QTD_RESERVED_BY_OMISSION = -1;
    private final static int QTD_DELIVERED_BY_OMISSION = -1;
    private final static int QTD_NOT_DELIVERED_BY_OMISSION=-1;

    public MealItem() {
    }

    public MealItem(Meal meal, int qtdPlanned) {
        this.meal = meal;
        this.qtdPlanned = qtdPlanned;
        this.qtdEffected = QTD_EFFECTED_BY_OMISSION;
        this.qtdReserved = QTD_RESERVED_BY_OMISSION;
        this.qtdDelivered = QTD_DELIVERED_BY_OMISSION;
        this.qtdNotDelivered = QTD_NOT_DELIVERED_BY_OMISSION;
    }

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(Long id) {
        return id.equals(this.id);
    }

    @Override
    public Long id() {
        return this.id;
    }

    public Meal meal() {
        return meal;
    }

    public int qtdPlanned() {
        return this.qtdPlanned;
    }

    public int qtdEffected() {
        return this.qtdEffected;
    }

    public int qtdReserved() {
        return this.qtdReserved;
    }

    public int qtdDelivered() {
        return this.qtdDelivered;
    }
    
    public int qtdNotDelivered(){
        return this.qtdNotDelivered;
    }

    
    public void QtdNotDelivered (int newQtdNotDelivered){
        this.qtdNotDelivered= newQtdNotDelivered;
    }
    
    
    public void changeQtdPlanned(int newQtdPlanned) {
        if (newQtdPlanned <= 0) {
            throw new IllegalArgumentException("Error Invalid Quantity");
        }
        this.qtdPlanned = newQtdPlanned;
    }
    
   public void changeQtdEffected(int newQtdEffected){
       if(newQtdEffected < 0){
           throw new IllegalArgumentException("Error Invalid Quantity");
       }
       this.qtdEffected=newQtdEffected;
   }
}
