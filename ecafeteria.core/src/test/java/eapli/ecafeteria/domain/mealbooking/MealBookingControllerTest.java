/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.application.MealBookingController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JOSENUNO
 */
public class MealBookingControllerTest {
    
    public MealBookingControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isValid method, of class MealBookingController.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        Calendar date = Calendar.getInstance();
        date.set(2017, 5, 18);
        MealBookingController instance = new MealBookingController();
        boolean result = instance.isValid(date, MealType.DINNER);
        assertEquals(true, result);
        
        date.set(2015, 3, 3);
        result = instance.isValid(date, MealType.DINNER);
        assertEquals(false, result);
    }

    /**
     * Test of chooseMeal method, of class MealBookingController.
     */
    @Test
    public void testChooseMeal() {
        System.out.println("chooseMeal");
        Dish d = new Dish("Teste", 30f, 20f, 20f, 20f, new DishType("Teste", "Desc"));
        Meal meal = new Meal(Calendar.getInstance(), d, MealType.DINNER);
        Set<RoleType> r = new AbstractSet<RoleType>() {
            @Override
            public Iterator<RoleType> iterator() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            @Override
            public int size() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        CafeteriaUser cafeteriaUser = new CafeteriaUser(new SystemUser("teste", "teste", "a", "a", "a@esa.com", r),
                new OrganicUnit("teste", "teste", "teste"), "teste");
        UserCard u = new UserCard();
        u.setBalance(50f);
        cafeteriaUser.setUserCard(u);
        Calendar date = Calendar.getInstance();
        date.set(2017, 5, 18);
        MealBookingController instance = new MealBookingController();
        
        boolean result = instance.chooseMeal(meal, cafeteriaUser, date);
        assertEquals(true, result);
        
        assertEquals(20f, u.getBalance());
    }
}
