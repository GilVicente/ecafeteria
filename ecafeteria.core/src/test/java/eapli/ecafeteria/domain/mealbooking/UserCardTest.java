/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.sales.Movement;
import eapli.ecafeteria.domain.sales.MovementType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brafa
 */
public class UserCardTest {

    public UserCardTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of showBalance method, of class UserCard.
     */
    @Test
    public void testShowBalance() {
        System.out.println("showBalance");
        UserCard instance = new UserCard();
        CafeteriaUser id = new CafeteriaUser();
        instance.registerCredits(10, new Movement(MovementType.Charge, id));
        float expResult = 10.0F;
        float result = instance.showBalance();
        assertEquals(expResult, result, 0.0);

    }

}
