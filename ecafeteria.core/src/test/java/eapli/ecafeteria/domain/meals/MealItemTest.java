/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fabio
 */
public class MealItemTest {
    
    public MealItemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changeQtdPlanned method, of class MealItem.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testChangeQtdPlanned() {
        System.out.println("changeQtdPlanned");
        int newQtdPlanned = 0;
        MealItem instance = new MealItem();
        instance.changeQtdPlanned(newQtdPlanned);
    }
    /**
     * Test of changeQtdEffected method, of class MealItem.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testChangeQtdEffected() {
        System.out.println("changeQtdEffected");
        int newQtdEffected = -1;
        MealItem instance = new MealItem();
        instance.changeQtdEffected(newQtdEffected);
    }
}
