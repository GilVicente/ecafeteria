/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.EmailAddress;
import eapli.ecafeteria.domain.authz.Name;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.RoleSet;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.ecafeteria.domain.mealbooking.BookingStatesEnum;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Lacerda
 */
public class RegisterMealDeliveryControllerIT {
    
    public RegisterMealDeliveryControllerIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validateUserID method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testValidateUserID() throws DataIntegrityViolationException {
        
        System.out.println("validateUserID");
        
        Username username = new Username("a");
        Password password = new Password("a");
        Name name = new Name("a","b");
        EmailAddress email = new EmailAddress("a@hotmail.com");
        RoleSet r = new RoleSet();
        SystemUser systemUser= new SystemUser(username, password, name,email,r);
        OrganicUnit organicUnit = new OrganicUnit("a", "b", "c");
        MecanographicNumber mecanumber = new MecanographicNumber("123");
        CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers();
        CafeteriaUser cafU = new CafeteriaUser(systemUser,organicUnit,mecanumber);       
        repo.add(cafU);
        
        MecanographicNumber mecaNumber = new MecanographicNumber("123");
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
        boolean expResult = true;
        boolean result = instance.validateUserID(mecaNumber);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of createListBookingController method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testCreateListBookingController() {
        System.out.println("createListBookingController");
       
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
        ListBookingsController expResult = new ListBookingsController();
        ListBookingsController result = instance.createListBookingController();
        assertTrue(expResult instanceof ListBookingsController && result instanceof ListBookingsController);
        
    }

    /**
     * Test of getBookedBookings method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testGetBookedBookings() throws DataIntegrityViolationException {
        System.out.println("getBookedBookings");
        
          
        Username username = new Username("a");
        Password password = new Password("a");
        Name name = new Name("a","b");
        EmailAddress email = new EmailAddress("a@hotmail.com");
        RoleSet r = new RoleSet();
        SystemUser systemUser= new SystemUser(username, password, name,email,r);
        OrganicUnit organicUnit = new OrganicUnit("a", "b", "c");
        MecanographicNumber mecanumber = new MecanographicNumber("123");
        CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers();
        CafeteriaUser cafU = new CafeteriaUser(systemUser,organicUnit,mecanumber);       
        repo.add(cafU);
        
        Calendar BookingDate= new GregorianCalendar(12, 12, 12);
        Meal meal = new Meal ();
        Booking b = new Booking(BookingDate, cafU, meal);
        b.setState(new BookingState(BookingStatesEnum.Booked));
        
        b.setUser(cafU);
        
        
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
        instance.validateUserID(mecanumber);
        ListBookingsController lbc = instance.createListBookingController();
        
        ArrayList<Booking> expResult = lbc.getUserBookings(cafU);
        ArrayList<Booking> result = instance.getBookedBookings();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of updateBookingState method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testUpdateBookingState() throws DataIntegrityViolationException {
        System.out.println("updateBookingState");
        
          Username username = new Username("a");
        Password password = new Password("a");
        Name name = new Name("a","b");
        EmailAddress email = new EmailAddress("a@hotmail.com");
        RoleSet r = new RoleSet();
        SystemUser systemUser= new SystemUser(username, password, name,email,r);
        OrganicUnit organicUnit = new OrganicUnit("a", "b", "c");
        MecanographicNumber mecanumber = new MecanographicNumber("123");
        CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers();
        CafeteriaUser cafU = new CafeteriaUser(systemUser,organicUnit,mecanumber);       
        repo.add(cafU);
        
        
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
          Calendar BookingDate= new GregorianCalendar(12, 12, 12);
        Meal meal = new Meal ();
        Booking b = new Booking(BookingDate, cafU, meal);
        b.setState(new BookingState(BookingStatesEnum.Booked));
        instance.setBooking(b);
        instance.updateBookingState();
        assertTrue(b.getState() instanceof BookingState);
        
    }

    /**
     * Test of registerDelivery method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testRegisterDelivery() {
        System.out.println("registerDelivery");
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
        boolean expResult = false;
        boolean result = instance.registerDelivery();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBooking method, of class RegisterMealDeliveryController.
     */
    @Test
    public void testSetBooking() {
        System.out.println("setBooking");
        Booking b = null;
        RegisterMealDeliveryController instance = new RegisterMealDeliveryController();
        instance.setBooking(b);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}

