/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealItem;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gil
 */
public class RegisterCookedMealsControllerTest {
    
    public RegisterCookedMealsControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListOfMealsByDay method, of class RegisterCookedMealsController.
     */
    @Test
    public void testGetListOfMealsByDay() {
        System.out.println("getListOfMealsByDay");
        RegisterCookedMealsController instance = new RegisterCookedMealsController();
        List<MealItem> res = instance.getListOfMealsByDay();
        
        boolean expResult = true;
        boolean result = false;
        if(res != null) result = true;
        assertEquals(expResult, result);
    }

    /**
     * Test of insertCookedMeals method, of class RegisterCookedMealsController.
     */
    @Test
    public void testInsertCookedMeals() {
        System.out.println("insertCookedMeals");
        
        DishType dishType = new DishType("Vegie", "Cool");
        Dish dish = new Dish("Bacalhau", 5, 5, 5, 5, dishType);
        Calendar cal =  Calendar.getInstance();
        Meal meal = new Meal(cal, dish, MealType.LUNCH);
        int qtdPlanned = 10;
        MealItem mItem = new MealItem(meal, qtdPlanned);
        
        int quantityCooked = 19;
        RegisterCookedMealsController instance = new RegisterCookedMealsController();
        instance.insertCookedMeals(mItem, quantityCooked);
        
        boolean expResult = true;
        boolean result = false;
        if(mItem.qtdEffected() == quantityCooked) result = true;
        assertEquals(expResult, result);
    }
    
}
