/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.BookingState;
import eapli.ecafeteria.domain.mealbooking.BookingStatesEnum;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class CancelBookingControllerTest {
    
    public CancelBookingControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of cancelBooking method, of class CancelBookingController.
     */
    @Test
    public void testCancelBooking() {
        System.out.println("cancelBooking");
        Booking booking = new Booking();
      //  CafeteriaUser cUser = new CafeteriaUser();
        booking.setState(new BookingState(BookingStatesEnum.Booked));
        CancelBookingController instance = new CancelBookingController();
        //instance.cancelBooking(booking,cUser);
        boolean expResult = true;
        boolean result = BookingStatesEnum.Canceled.equals(booking.getState().getState());
        assertEquals(expResult, result);
    }
    
}
