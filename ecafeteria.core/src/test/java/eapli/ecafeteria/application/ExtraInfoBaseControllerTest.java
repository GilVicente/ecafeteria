/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.EmailAddress;
import eapli.ecafeteria.domain.authz.Name;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.Role;
import eapli.ecafeteria.domain.authz.RoleSet;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.mealbooking.UserCard;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class ExtraInfoBaseControllerTest {
    
    public ExtraInfoBaseControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCurrentBalance method, of class ExtraInfoBaseController.
     */
    @Test
    public void testGetCurrentBalance1() {
        System.out.println("getCurrentBalance");
        Name nomeUtilizador=new Name("Guilherme","Sousa");
        Username username= new Username("GuiFMRS");
        Password pass= new Password("pass123");
        EmailAddress email=new EmailAddress("guifmrs@gmail.com");
        RoleSet role=new RoleSet();
        RoleType userRole=RoleType.User;
        Role realRole=new Role(userRole);
        role.add(realRole);
        SystemUser user= new SystemUser(username,pass,nomeUtilizador,email,role);
        OrganicUnit ou= new OrganicUnit("acronym","name","description");
        MecanographicNumber mn= new MecanographicNumber("1130638");
        UserCard userCard=new UserCard();
        userCard.setBalance(2500);
        CafeteriaUser cu=new CafeteriaUser(user,ou,mn);
        cu.setUserCard(userCard);
        double expResult = 3500.0;
        double result = cu.seeCredits();
        assertNotEquals(expResult, result, 0.0);

    }
    
    @Test
    public void testGetCurrentBalance2() {
        System.out.println("getCurrentBalance");
        Name nomeUtilizador=new Name("Guilherme","Sousa");
        Username username= new Username("GuiFMRS");
        Password pass= new Password("pass123");
        EmailAddress email=new EmailAddress("guifmrs@gmail.com");
        RoleSet role=new RoleSet();
        RoleType userRole=RoleType.User;
        Role realRole=new Role(userRole);
        role.add(realRole);
        SystemUser user= new SystemUser(username,pass,nomeUtilizador,email,role);
        OrganicUnit ou= new OrganicUnit("acronym","name","description");
        MecanographicNumber mn= new MecanographicNumber("1130638");
        UserCard userCard=new UserCard();
        userCard.setBalance(150);
        CafeteriaUser cu=new CafeteriaUser(user,ou,mn);
        cu.setUserCard(userCard);
        double expResult = 150.0;
        double result = cu.seeCredits();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getNextReservedMeal method, of class ExtraInfoBaseController.
     */
    @Test
    public void testGetNextReservedMeal() {
        System.out.println("getNextReservedMeal");
        Name nomeUtilizador=new Name("Guilherme","Sousa");
        Username username= new Username("GuiFMRS");
        Password pass= new Password("pass123");
        EmailAddress email=new EmailAddress("guifmrs@gmail.com");
        RoleSet role=new RoleSet();
        RoleType userRole=RoleType.User;
        Role realRole=new Role(userRole);
        role.add(realRole);
        SystemUser user= new SystemUser(username,pass,nomeUtilizador,email,role);
        OrganicUnit ou= new OrganicUnit("acronym","name","description");
        MecanographicNumber mn= new MecanographicNumber("1130638");
        UserCard userCard=new UserCard();
        userCard.setBalance(150);
        CafeteriaUser cu=new CafeteriaUser(user,ou,mn);
        cu.setUserCard(userCard);
        DishType dType1=new DishType("Nome","Descrição");
        DishType dType2=new DishType("Nome do dish type","Descrição do dish type");
        DishType dType3=new DishType("Nome do dish type 3","Descrição do dish type 3");
        Dish d1= new Dish("Prato 1",22,2.2,0.1,0.05,dType1);
        Dish d2= new Dish("Prato 2",10,1.1,1.0,2.0,dType2);
        Dish d3= new Dish("Prato 3",5,0.0,0.0,0.0,dType3);
        Dish d4= new Dish("Prato 4",30,3.0,1.5,3.0,dType2);
        Meal m1= new Meal(new GregorianCalendar(2015,7,20),d1,MealType.LUNCH);
        Meal m2= new Meal(new GregorianCalendar(2015,7,20),d2,MealType.DINNER);
        Meal m3= new Meal(new GregorianCalendar(2015,7,21),d3,MealType.LUNCH);
        Meal m4= new Meal(new GregorianCalendar(2015,7,21),d4,MealType.DINNER);
        Booking b1= new Booking(new GregorianCalendar(2015,7,19),cu,m1);
        Booking b2= new Booking(new GregorianCalendar(2015,7,15),cu,m2);
        Booking b3= new Booking(new GregorianCalendar(2015,7,20),cu,m3);
        Booking b4= new Booking(new GregorianCalendar(2015,7,18),cu,m4);
        List<Booking> bookings = new ArrayList<>();
        bookings.add(b2);
        bookings.add(b4);
        bookings.add(b1);
        bookings.add(b3);
        Meal expResult = m2;
        Meal result = bookings.iterator().next().getMeal();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetNextReservedMeal2() {
        System.out.println("getNextReservedMeal");
        Name nomeUtilizador=new Name("Guilherme","Sousa");
        Username username= new Username("GuiFMRS");
        Password pass= new Password("pass123");
        EmailAddress email=new EmailAddress("guifmrs@gmail.com");
        RoleSet role=new RoleSet();
        RoleType userRole=RoleType.User;
        Role realRole=new Role(userRole);
        role.add(realRole);
        SystemUser user= new SystemUser(username,pass,nomeUtilizador,email,role);
        OrganicUnit ou= new OrganicUnit("acronym","name","description");
        MecanographicNumber mn= new MecanographicNumber("1130638");
        UserCard userCard=new UserCard();
        userCard.setBalance(150);
        CafeteriaUser cu=new CafeteriaUser(user,ou,mn);
        cu.setUserCard(userCard);
        DishType dType1=new DishType("Nome","Descrição");
        DishType dType2=new DishType("Nome do dish type","Descrição do dish type");
        DishType dType3=new DishType("Nome do dish type 3","Descrição do dish type 3");
        Dish d1= new Dish("Prato 1",22,2.2,0.1,0.05,dType1);
        Dish d2= new Dish("Prato 2",10,1.1,1.0,2.0,dType2);
        Dish d3= new Dish("Prato 3",5,0.0,0.0,0.0,dType3);
        Dish d4= new Dish("Prato 4",30,3.0,1.5,3.0,dType2);
        Meal m1= new Meal(new GregorianCalendar(2015,7,20),d1,MealType.LUNCH);
        Meal m2= new Meal(new GregorianCalendar(2015,7,20),d2,MealType.DINNER);
        Meal m3= new Meal(new GregorianCalendar(2015,7,21),d3,MealType.LUNCH);
        Meal m4= new Meal(new GregorianCalendar(2015,7,21),d4,MealType.DINNER);
        Booking b1= new Booking(new GregorianCalendar(2015,7,19),cu,m1);
        Booking b2= new Booking(new GregorianCalendar(2015,7,15),cu,m2);
        Booking b3= new Booking(new GregorianCalendar(2015,7,20),cu,m3);
        Booking b4= new Booking(new GregorianCalendar(2015,7,18),cu,m4);
        List<Booking> bookings = new ArrayList<>();
        bookings.add(b2);
        bookings.add(b4);
        bookings.add(b1);
        bookings.add(b3);
        bookings.remove(b2);
        Meal expResult = m4;
        Meal result = bookings.iterator().next().getMeal();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetNextReservedMeal3() {
        System.out.println("getNextReservedMeal");
        Name nomeUtilizador=new Name("Guilherme","Sousa");
        Username username= new Username("GuiFMRS");
        Password pass= new Password("pass123");
        EmailAddress email=new EmailAddress("guifmrs@gmail.com");
        RoleSet role=new RoleSet();
        RoleType userRole=RoleType.User;
        Role realRole=new Role(userRole);
        role.add(realRole);
        SystemUser user= new SystemUser(username,pass,nomeUtilizador,email,role);
        OrganicUnit ou= new OrganicUnit("acronym","name","description");
        MecanographicNumber mn= new MecanographicNumber("1130638");
        UserCard userCard=new UserCard();
        userCard.setBalance(150);
        CafeteriaUser cu=new CafeteriaUser(user,ou,mn);
        cu.setUserCard(userCard);
        DishType dType1=new DishType("Nome","Descrição");
        DishType dType2=new DishType("Nome do dish type","Descrição do dish type");
        DishType dType3=new DishType("Nome do dish type 3","Descrição do dish type 3");
        Dish d1= new Dish("Prato 1",22,2.2,0.1,0.05,dType1);
        Dish d2= new Dish("Prato 2",10,1.1,1.0,2.0,dType2);
        Dish d3= new Dish("Prato 3",5,0.0,0.0,0.0,dType3);
        Dish d4= new Dish("Prato 4",30,3.0,1.5,3.0,dType2);
        Meal m1= new Meal(new GregorianCalendar(2015,7,20),d1,MealType.LUNCH);
        Meal m2= new Meal(new GregorianCalendar(2015,7,20),d2,MealType.DINNER);
        Meal m3= new Meal(new GregorianCalendar(2015,7,21),d3,MealType.LUNCH);
        Meal m4= new Meal(new GregorianCalendar(2015,7,21),d4,MealType.DINNER);
        Booking b1= new Booking(new GregorianCalendar(2015,7,19),cu,m1);
        Booking b2= new Booking(new GregorianCalendar(2015,7,15),cu,m2);
        Booking b3= new Booking(new GregorianCalendar(2015,7,20),cu,m3);
        Booking b4= new Booking(new GregorianCalendar(2015,7,18),cu,m4);
        List<Booking> bookings = new ArrayList<>();
        bookings.add(b2);
        bookings.add(b4);
        bookings.add(b1);
        bookings.add(b3);
        bookings.remove(b2);
        bookings.remove(b4);
        Meal expResult = m1;
        Meal result = bookings.iterator().next().getMeal();
        assertEquals(expResult, result);
    }
    
}
