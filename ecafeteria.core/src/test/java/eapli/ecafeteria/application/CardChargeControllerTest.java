/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brafa
 */
public class CardChargeControllerTest {

    public CardChargeControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of registerCredits method, of class CardChargeController.
     */
    @Test
    public void testRegisterCredits() {
        System.out.println("registerCredits");
        float credits = 10.0F;
        final Set<RoleType> roles = new HashSet<RoleType>();
        roles.add(RoleType.Cashier);
        MecanographicNumber mec = new MecanographicNumber("asdwresa");
        CafeteriaUser c = new CafeteriaUser(new SystemUser("asd", "asd", "asd", "asd", "johny.doe@emai.l.com", roles), new OrganicUnit("asd", "asd", "asd"), mec);

        CardChargeController instance = new CardChargeController();
        instance.cafeteriaUser(c);
        boolean expResult = true;
        boolean result = instance.registerCredits(credits);
        assertEquals(expResult, result);
        assertEquals(credits, c.seeCredits(), 0.0);

    }


}
