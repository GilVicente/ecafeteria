/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.MealBookingController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author hugoc
 */
public class MealBookingUI extends AbstractUI{
    
    private final MealBookingController controller= new MealBookingController();
    
    
    
    @Override
    protected boolean doShow() {
        Calendar date;
        
        int tam = MealType.values().length;
        
        System.out.println("Select a Meal Type");
        for (int i = 0; i < tam; i++) System.out.println(String.format("%d) %s", i, MealType.values()[i]));
        
        int mealTypeInt;
        do{
          mealTypeInt = Console.readInteger("");
        }while (mealTypeInt < 0 || mealTypeInt >= tam);
        
        MealType mealType = MealType.values()[mealTypeInt];
        
        
        do{
        final String bookingDate = Console.readLine("Booking date(dd-mm-yyyy):");
        String dateString [] = bookingDate.split("-");
        
        date = Calendar.getInstance();
        date.set(Integer.parseInt(dateString[2]), 
                 Integer.parseInt(dateString[1]),
                 Integer.parseInt(dateString[0]));
        
        }while(!controller.isValid(date, mealType));
   
        ArrayList<Meal> meals = controller.getMenu(date, mealType);
        int i=0;
        if(meals!=null &&meals.size()!=0){
        for (Meal meal : meals) {
            System.out.println(String.format("%d) %s",++i,meal.toString()));
        }
        int mealNumber;
        do{
            mealNumber = Console.readInteger("Choose your meal:");
        }while(mealNumber<=0 && mealNumber>i);
        Meal finalMeal = meals.get(mealNumber--);
        
        SystemUser su = AppSettings.instance().session().authenticatedUser();
        
        
        CafeteriaUser cUser = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(su);
        
        if(controller.chooseMeal(finalMeal, cUser, date))
            System.out.println("Meal booked");
        }
        System.out.println("No meals available");

        return false;
    }

    @Override
    public String headline() {
        return "Meal Booking";
    }

}
