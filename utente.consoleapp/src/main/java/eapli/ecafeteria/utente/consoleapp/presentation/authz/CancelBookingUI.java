/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.CancelBookingController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hugoc
 */
public class CancelBookingUI extends AbstractUI{
    
    private CancelBookingController controller;
    
    @Override
    protected boolean doShow() {
        this.controller= new CancelBookingController();
        List<String> bl = new ArrayList();
        
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        bl = this.controller.getBookings(user);
        int i=0;
        if(bl!=null&& bl.size()!=0){
        for ( i= 0;i<bl.size() ;) {
            System.out.println(String.format("%d) %s", i++,bl.get(i-1).toString()));
        }
        int option=-1;
        do{
        option= Console.readInteger("Booking to cancel:");
        }while(option<0 || option>i);
        CafeteriaUser cUser=PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(user);
        //this.controller.cancelBooking(bl.get(option), cUser);
        //System.out.println("Booking was canceled");
        }
        else
        System.out.println("No bookings to cancel");
        
    return true;
    }

    @Override
    public String headline() {
        return("Cancel a Booking");    
    }
    
}
