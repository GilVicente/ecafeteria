/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.*;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.utente.consoleapp.presentation.authz.*;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int MY_RESERVATIONS_OPTION = 2;
    private static final int MY_CARDACCOUNT_OPTION = 3;
    private static final int MY_RATINGSCALORIES_OPTION = 4;
    
    //MAIN MENU RESERVATION
    private static final int LIST_N_DAYS_RESERVATION=1;
    private static final int BOOK_MEAL =2;
    private static final int CANCEL_BOOKING=3;

    /*
     * Option not being used private static final int DISH_TYPE_OPTION = 2;
     */
    public MainMenu() {
    }

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu);
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
//        final Menu myReservationsMenu = new MyReservationsMenu();
//        final Menu myCardAccountMenu = new MyCardAccountMenu();
//        final Menu myRatingsCaloriesMenu = new MyRatingsCaloriesMenu();
        
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));
        Menu myReservationsMenu = buildMyReservationsMenu();
        mainMenu.add(new SubMenu(MY_RESERVATIONS_OPTION, myReservationsMenu, new ShowVerticalSubMenuAction(myReservationsMenu)));
//        mainMenu.add(new SubMenu(MY_CARDACCOUNT_OPTION, myCardAccountMenu, new ShowVerticalSubMenuAction(myCardAccountMenu)));
//        mainMenu.add(new SubMenu(MY_RATINGSCALORIES_OPTION, myRatingsCaloriesMenu, new ShowVerticalSubMenuAction(myRatingsCaloriesMenu)));
        
        mainMenu.add(VerticalSeparator.separator());

        // TODO add menu options

        mainMenu.add(VerticalSeparator.separator());

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildMyReservationsMenu() {
        final Menu menu = new Menu("Reservation Menu >");

        menu.add(new MenuItem(LIST_N_DAYS_RESERVATION, "List n days of reservation", new QueryMealBookingAction()));
            // example of using the generic list ui from the framework
        menu.add(new MenuItem(BOOK_MEAL,"Book a meal", new MealBookingAction()));
        menu.add(new MenuItem(CANCEL_BOOKING,"Cancel a booked meal", new CancelBookingAction()));
       
        // TODO add other options for Organic Unit management
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }
}
