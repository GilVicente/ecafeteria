/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.QueryMealBookingController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.autenUser;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;

/**
 *
 * @author dmaia
 */
public class QueryMealBookingUI extends AbstractUI{
    
    QueryMealBookingController controller = new QueryMealBookingController();
    
    @Override
    protected boolean doShow() {
       
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        CafeteriaUser cafUser = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(user);
                
        int nDays = Integer.parseInt(Console.readLine("Number of days:"));

        ArrayList<Booking> bookings = controller.getBookingPerDays(nDays);
        int i = 0;
        if (bookings!=null) {
        for (Booking booking : bookings) {
            System.out.println(String.format("%d) %s", ++i, booking.toString()));
        }
        }else{
            System.out.println("Not exist bookings!!!");    
        }
        return false;

    }

    @Override
    public String headline() {
        
        return "List n days of reservation";
    }
    
}
